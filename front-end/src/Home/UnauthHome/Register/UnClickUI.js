import React from 'react';
//import Logo from '../../../image/logo.png'
import Balance from '../../../image/balance.png'
import FileIcon from '../../../image/file.png'
import Folder from '../../../image/folder.png'
import FacebookLogo from '../../../image/facebook.png'
import WechatLogo from '../../../image/icons8-weixin-48.png'
import InstagtamLogo from '../../../image/instagram.png'
import TwitterLogo from '../../../image/twitter.png'

export default function UnClickUI(props) {
    return(
        <div>
            <div className="register-button" onClick={props.switchRegister}>Create Account →</div>
            <div className="row text-center">
                <div className="col register-image"><img src={Balance} alt="balance"/><h4><b>Profile Picture</b></h4><p className="text-white">Upload your own profile picture and displayed as an avatar next to the account name</p></div>
                <div className="col register-image"><img src={Folder} alt="folder"/><h4><b>Course Enrollment</b></h4><p className="text-white">Manipulate your own course Enrollment with a max credit limit</p></div>
                <div className="col register-image"><img src={FileIcon} alt="file"/><h4><b>Detail Report</b></h4><p className="text-white">LMS allows you to setup different usertype for your account, different usertype has different rights and permissions </p></div>
            </div>
                
            <div className="intro-container">
                <h4 className="text-center">Welcome to Marsden</h4><p className="text-center">A Learning Manage System For Marsden High School</p>
                {/* <p className="text-center">A Learning Manage System For Marsden High School</p> */}
                <div className="text-center mt-3">
                    <img className="social-icon fb-icon" src={FacebookLogo} alt="facebook icon" />
                    <img className="social-icon weixin-icon" src={WechatLogo} alt="wechat icon" />
                    <img className="social-icon ins-icon" src={InstagtamLogo} alt="instagram icon" />
                    <img className="social-icon twitter-icon" src={TwitterLogo} alt="twitter icon" />
                </div>
            </div>  
        </div>
    );
}
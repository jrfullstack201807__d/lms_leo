import React,{Component} from 'react';
import './Register.css';
import UnClickUI from './UnClickUI';
import Phone from '../../../image/phone.png'
import H from '../../../image/h.png'
import Tick from '../../../image/tick.png'
import MarsdenLogo from '../../../image/marsdenlogo.jpg'
import {postSignUp} from '../../../API/LMS';



export default class Register extends Component{
    constructor(props){
        super(props)
        this.state={
            account:{value:"",valid:true,message:"Error! Account name must contain at least 8 characters."},
            password:{value:"",valid:true},
            name:{value:"",valid:true},
            phone:{value:"",valid:true},
            type:{value:"",valid:true},
            submit:"submit",
            onRegister: false
        }
    }
    onUserChange(e){
        this.setState({account:{value:e.target.value,valid:true,message:"Error! Account name must contain at least 8 characters."}})
    }
    
    onPasswordChange(e){
        this.setState({password:{value:e.target.value,valid:true}})
    }

    onNameChange(e){
        this.setState({name:{value:e.target.value,valid:true}})
    }
    
    onPhoneChange(e){
        this.setState({phone:{value:e.target.value,valid:true}})
    }

    onRadioChange(e){
        this.setState({type:{value:e.target.value,valid:true}})
    }

    onSubmit(e){
        e.preventDefault();
        this.setState({
            submit:"button"
        })
        if(this.validation()){
            let user = {
                Name:this.state.name.value,
                UserType:this.state.type.value,
                Account:this.state.account.value,
                Password:this.state.password.value,
                Phone:this.state.phone.value
            }
            
            postSignUp(user).then(data=>this.handleResult(data)).catch(function (error) {
               console.log(error);
              })
        }else{
            this.setState({
                        submit:"submit"
                    })
        }
    }

    switchRegister(){
        this.setState({
            onRegister: !this.state.onRegister
        })
        this.props.switchRegister();
    }

    handleResult(data){
        if(data.Status === 0){
            this.setState({
                account:{value:"",valid:false,message:data.Message},
                password:{value:"",valid:true},
                name:{value:"",valid:true},
                phone:{value:"",valid:true},
                type:{value:this.state.type.value, valid:true},
                submit:"submit"
            })
        }else{
            alert(data.Message);
            this.setState({
                account:{value:"",valid:true,message:"Error! Account name must contain at least 8 characters. "},
                password:{value:"",valid:true},
                name:{value:"",valid:true},
                phone:{value:"",valid:true},
                type:{value:this.state.type.value, valid:true},
                submit:"submit"
            })
        }
    }

    validation(){
        let isValid=true;
        let i = /[0-9]+/;
        let str = /[A-Za-z]/;
        let spec=/[,.<>{}~!@#$%^&*]/;
        let account=this.state.account.value;
        let password=this.state.password.value;
        let name=this.state.name.value;
        let phone=this.state.phone.value;
        let type=this.state.type.value;

        if(account.length<8){
            this.setState({account:{value: account,valid:false,message:"Error! Account name must contain at least 8 characters."}});
            isValid=false;
        }
        if(password.length<8){
            this.setState({password:{value: password,valid:false}});
            isValid=false;
        }
        if(i.test(name)||spec.test(name)||name === ""){
            this.setState({name:{value: name,valid:false}});
            isValid=false;
        }
        if(phone.length!==10||str.test(phone)){
            this.setState({phone:{value: phone,valid:false}});
            isValid=false;
        }
        if(type === ''){
            this.setState({type:{value: type,valid:false}});
            isValid=false;
        }
        return isValid;
    }

    render(){
        return(
        <div className="container">
            {
                this.state.onRegister ? 
                <div className="row  register-box position-relative">
                    <i className="far fa-times-circle close-register-button" onClick={this.switchRegister.bind(this)}/>
                    <form className="col container" onSubmit={this.onSubmit.bind(this)}>
                    
                        <div className="text-center"><img className="logo-image-marsden" src={MarsdenLogo} alt="logo"/></div>
                        <h4 className="text-center sign-up-now">Create an Account !</h4>
                        <div className="input-wrapper"><i className="fa fa-user-circle" aria-hidden="true" ></i><input  className="input-box row ml-2" type="text" name="user_account" autoComplete="username" value={this.state.account.value} placeholder="AccountName" onChange={this.onUserChange.bind(this)}/></div>
                        {this.state.account.valid ? '':<p className="error-message" >{this.state.account.message}</p>}
                        <div className="input-wrapper"><i className="fa fa-key" aria-hidden="true"></i><input className="input-box row ml-2" type="password" autoComplete="current-password" name="user_password" value={this.state.password.value} placeholder="Password" onChange={this.onPasswordChange.bind(this)}/></div>
                        {this.state.password.valid ? '':<p className="error-message" >Error! Password must contain at least 8 characters. </p>}
                        <div className="input-wrapper"><i className="fa fa-font" aria-hidden="true"></i><input className="input-box row ml-2" type="text" name="user_name" value={this.state.name.value} placeholder="NickName" onChange={this.onNameChange.bind(this)}/></div>
                        {this.state.name.valid ? '':<p className="error-message" >Error! Name only contain letters.</p>}
                        <div className="input-wrapper"><i className="fa fa-phone" aria-hidden="true"></i><input className="input-box row ml-2" type="text" name="user_phone" value={this.state.phone.value} placeholder="Phone" onChange={this.onPhoneChange.bind(this)}/></div>
                        {this.state.phone.valid ? '':<p className="error-message" >Error! Incorrect number. </p>}
                        <div className="mx-auto"onChange={this.onRadioChange.bind(this)}>
                            <label className="mx-2 d-block">User Type:</label>
                            <div className="text-center">
                                <input className="mx-2" type="radio" value="Admin" name="user_type" id="user_admin"/><label htmlFor="user_admin">Admin</label>
                                <input className="mx-2 " type="radio" value="Student" name="user_type" id="user_student"/><label htmlFor="user_student">Student</label>
                                <input className="mx-2 " type="radio" value="Lecturer" name="user_type" id="user_lecturer"/><label htmlFor="user_lecturer">Lecturer</label>
                            </div>
                        </div>
                        {this.state.type.valid ? '':<p  className="error-message" >Error! Choose one of the user types. </p>}
                        <button className="signup-button" type={this.state.submit}>Sign Up</button>
                    </form>

                    <div className="register-box-right col-md d-md-block text-center">
                        <div className="register-image step-image">
                        <img src={H} alt="h"/>
                        <h4><b>STEP 1 : </b></h4><p className="text-white">Fill out the form </p>
                        </div>
                        <div className="register-image step-image"><img src={Tick} alt="tick"/><h4><b>STEP 2 : </b></h4><p className="text-white">Check the validation of form</p></div>
                        <div className="register-image step-image"><img src={Phone} alt="phone"/><h4><b>STEP 3 : </b></h4><p className="text-white">Receive Verfication code from SMS</p></div>
                    </div>
                </div> 
                :
                <UnClickUI switchRegister={this.switchRegister.bind(this)}/>
            }
        </div>
        )
    }
}
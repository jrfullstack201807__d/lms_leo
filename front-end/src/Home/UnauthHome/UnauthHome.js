import React,{Component} from 'react';
import Login from './Login/Login';
import Register from './Register/Register';

export default class UnauthHome extends Component{
    constructor(props){
        super(props);
        this.state={
            className: "body-container background-img"
        }
    }

    switchRegister(){
        if(this.state.className === "body-container background-img"){
            this.setState({
                className: "body-container background-img-shadow"
            })
        }else{
            this.setState({
                className: "body-container background-img"
            })
        }
    }

    render(){
        return(
            <div className={this.state.className}>
                <Login successLogIn={this.props.successLogIn} />
                <Register switchRegister={this.switchRegister.bind(this)} />
            </div>
        );
    }
}
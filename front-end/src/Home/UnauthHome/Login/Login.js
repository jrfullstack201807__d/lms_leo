import React,{Component} from 'react'
import './Login.css'
import {postLogIn} from '../../../API/LMS'

export default class Login extends Component{
    constructor(props){
        super(props)
        this.state={
            account:"",
            password:"",
            verification:"",
            displayVerification: false,
            message:"",
            valid:true,
            submit:"submit"
        }
    }
    onUserChange(e){
        this.setState({account:e.target.value})
    }
    
    onPasswordChange(e){
        this.setState({password:e.target.value})
    }

    onNumberChange(e){
        this.setState({verification:e.target.value})
    }

    onSubmit(e){
        e.preventDefault();
        this.setState({submit:"button"})
        if(this.validation()){
                let user = {Account:this.state.account,Password:this.state.password,VerificationNumber:this.state.verification};
                postLogIn(user).then(data=>this.handleResult(data)).catch(function (error) {
                    console.log(error);})
        }else{
            this.setState({
                valid:false,
                message:"Incorrect Username or Password",
                account:"",
                password:"",
                verification:"",
                submit:"submit"
            });
        }
        
    }

    validation(){
        if(this.state.account.length<8 || this.state.password.length<8){
            return false;
        }else{
            return true;
        }
    }

    handleResult(data){
        if(data.Status<0){
            this.setState({
                valid:false,
                message:data.Message,
                account:"",
                password:"",
                verification:"",
                submit:"submit"
            });
        }else if(data.Status === 0){
            let user = {Account:this.state.account,Password:this.state.password};
            postLogIn(user).then(data=>this.handleResult(data))
        }else if(data.Status === 1){
            //add token to session
            let token = data.Message;
            sessionStorage.setItem('key',token);
            this.setState({
                message:"",
                valid:true,
                account:"",
                password:"",
                verification:"",
                submit:"submit"
            });
            this.successLogIn();
        }else{
            alert("Error!")
        }
    }

    successLogIn(){
        this.props.successLogIn();
    }

    verificationSwitch(){
        this.setState({
            displayVerification: !this.state.displayVerification,
        })
    }

    render(){
        return (
        <div className="container navbar login-box ">
            <form className="row mx-auto" onSubmit={this.onSubmit.bind(this)}>
                {this.state.valid?'':<p className="error-message" >{this.state.message}</p>}
                <div className="col-s mx-auto text-center">
                    <input className=" input-box ml-1" type="text" name="user_account" autoComplete="username" value={this.state.account} placeholder="Username" onChange={this.onUserChange.bind(this)}/>
                    <input className=" input-box ml-sm-2 ml-1 password-input" type="password" autoComplete="current-password" name="user_password" value={this.state.password} placeholder="Password" onChange={this.onPasswordChange.bind(this)}/>
                </div>
                <div className="col-s mx-auto ">
                    {this.state.displayVerification ?
                    <div className="ml-2" >
                        <input className="input-box" type="text" name="user_verification" value={this.state.verification} placeholder="xxxx" onChange={this.onNumberChange.bind(this)}/>
                        <i className="far fa-times-circle" onClick={this.verificationSwitch.bind(this)}/>
                    </div>
                    :
                    <button className=" ml-2 confirm-button" onClick={this.verificationSwitch.bind(this)}>Verification</button>
                    }
                    <button className=" ml-2 confirm-button" type={this.state.submit}>Login</button>
                </div>
            </form>
        </div>
        )
    }
}

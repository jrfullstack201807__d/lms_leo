import React,{Component} from 'react';
import {withRouter} from 'react-router-dom';

class LogOut extends Component {
    
    logOut(){
        this.props.history.push('/');
        sessionStorage.removeItem('key');
        this.props.switchToUnauth();
    }

    render(){
        return(
            <div className="ml-2 mr-auto">
                <button className="confirm-button" onClick={this.logOut.bind(this)}>Log Out</button>
            </div>

        );
    }
}

export default withRouter(LogOut);

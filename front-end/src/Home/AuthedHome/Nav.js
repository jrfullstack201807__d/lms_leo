import React,{Component} from 'react';
import './AuthedHome.css'
import {NavLink} from 'react-router-dom';
import LogOut from './LogOut';

export default class Nav extends Component{
    constructor(props){
        super(props);
        this.state={
            onSwitch: false,
        }
    }
    userName = this.props.userInfo.Name;
    courseUrl = '/'+this.userName + '/course';
    studentUrl = '/'+this.userName + '/student';
    lecturerUrl = '/'+this.userName + '/lecturer';
    detailUrl = '/' + this.userName + '/detail'

    onSwitch(){
        this.setState({
            onSwitch: !this.state.onSwitch
        })
    }

    render(){
        return(
            <div className="navbar sticky-top navbar-light nav-bar">
            {
                this.state.onSwitch ?
                    <div className="side-menu-wrapper d-sm-none">
                        <div><i className="fas fa-book-open"></i><NavLink to={this.courseUrl}>Course</NavLink></div>
                        <div><i className="fas fa-user-graduate"></i><NavLink to={this.studentUrl}>Student</NavLink></div>
                        <div><i className="fas fa-chalkboard-teacher"></i><NavLink to={this.lecturerUrl}>Lecturer</NavLink></div>
                    </div>
                :
                ''
            }
                    <div className="container">
                        <NavLink to={this.detailUrl}><span>Personal Detail</span></NavLink>
                        <LogOut  switchToUnauth={this.props.switchToUnauth}/>
                        <div className="d-sm-none burger-menu-wrapper" onClick={this.onSwitch.bind(this)}>
                            <div className="burger-menu"></div>
                            <div className="burger-menu"></div>
                            <div className="burger-menu"></div>
                        </div>
                        <ul className="d-none d-sm-block">
                            <li><i className="fas fa-book-open"></i><NavLink to={this.courseUrl}>Course</NavLink></li>
                            <li><i className="fas fa-user-graduate"></i><NavLink to={this.studentUrl}>Student</NavLink></li>
                            <li><i className="fas fa-chalkboard-teacher"></i><NavLink to={this.lecturerUrl}>Lecturer</NavLink></li>
                        </ul>
                    </div>
                
            </div>
        );  
    }
}
import React,{Component} from 'react';
import {Route,Switch} from 'react-router-dom';
import Nav from './Nav';
import EnrolledDetail from '../../Container/EnrolledDetail';
import Welcome from '../../Container/Welcome';
import NotFound from '../../NotFound';
import CourseContainer from '../../Container/Course/CourseContainer';
import StudentContainer from '../../Container/Student/StudentContainer';
import LecturerContainer from '../../Container/Lecturer/LecturerContainer';

export default class AuthedHome extends Component{

    userName = this.props.userInfo.Name;
    detailUrl = '/' + this.userName + '/detail'
    courseUrl = '/' + this.userName + '/course';
    studentUrl = '/' + this.userName + '/student';
    lecturerUrl = '/' + this.userName + '/lecturer';

    isUserEnrolled = this.props.userInfo.IsEnrolled;

    render(){
        return (
            <div className="body-container">
                {
                    this.isUserEnrolled || this.props.userInfo.UserType === "Admin" ?
                    ''
                    :
                    <p className="border border-danger text-center">Warning, You Have To Enroll First Before Further Operation !</p>
                }
                    <Nav switchToUnauth={this.props.switchToUnauth} userInfo={this.props.userInfo}/>
                    <Switch>
                        <Route exact path="/" render={()=><Welcome userInfo={this.props.userInfo}/>} />
                        <Route exact path={this.courseUrl} render={()=><CourseContainer userInfo={this.props.userInfo}/>} />
                        <Route exact path={this.studentUrl} render={()=><StudentContainer userInfo={this.props.userInfo}/>}/>
                        <Route exact path={this.lecturerUrl} render={()=><LecturerContainer userInfo={this.props.userInfo}/>} />
                        <Route exact path={this.detailUrl} render={()=><EnrolledDetail refreshInfo={this.props.refreshInfo} userInfo={this.props.userInfo}/>}  />
                        <Route component={NotFound} />
                    </Switch>
                </div>
        )
    }
}


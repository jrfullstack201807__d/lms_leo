import React from 'react';
import {NavLink} from 'react-router-dom';

export default function LogInPrompt(){
    return(
        <div>
            <p>Please LogIn First</p><NavLink exact to="/"><button>Back To Home</button></NavLink>
        </div>
    )
}
import React from 'react';

export default function Message(props) {
    return(
        <div className="message-box">
            <h4>{props.message}</h4>
            <i className="far fa-times-circle" onClick={props.switchStatus}/>
        </div>
    )
}
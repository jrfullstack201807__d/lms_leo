import React,{Component} from 'react';
import {withRouter} from 'react-router-dom';

class TimeOut extends Component {
    
    backToHome(){
        this.props.switchToUnauth();
        this.props.history.push('/');
    }

    render(){
        return(
            <div className="body-container">
                <h2>TimeOut</h2>
                <p>Click Button Back to Home Page</p>
                <button onClick={this.backToHome.bind(this)}>Go Back</button>
            </div>

        );
    }
}

export default withRouter(TimeOut);
import axios from 'axios';

const host = "13.236.241.62"
//const host = "localhost:5000"

const LOGIN_URL="http://"+host+"/api/Users/LogIn";
const SIGNUP_URL="http://"+host+"/api/Users/SignUp";
const GET_USERNAME_URL="http://"+host+"/api/Users";
const POST_STUDENT_URL="http://"+host+"/api/Users/enrollStudent";
const POST_COURSE_URL="http://"+host+"/api/Users/AddCourse";
const UPDATE_COURSE_URL="http://"+host+"/api/Users/UpdateCourse";
const POST_LECTURER_URL="http://"+host+"/api/Users/enrollLecturer";
const POST_EMAIL_URL="http://"+host+"/api/Users/UpdateEmail";
const GET_ALL_COURSES="http://"+host+"/api/Users/allCourses";
const GET_ALL_LECTURERS="http://"+host+"/api/Users/allLecturers";
const GET_ALL_STUDENTS="http://"+host+"/api/Users/allStudents";
const BASED_STUDENTCOURSE_URL="http://"+host+"/api/Users/studentcourse";
const BASED_LECTURERCOURSE_URL="http://"+host+"/api/Users/lecturercourse";
const GET_COURSE_DETAIL="http://"+host+"/api/Users/CourseDetail";
const GET_STUDENTS_FOR_COURSE="http://"+host+"/api/Users/StudentsForCourse";
const GET_LECTURERS_FOR_COURSE="http://"+host+"/api/Users/LecturersForCourse";
const GET_COURSES_FOR_LECTURER="http://"+host+"/api/Users/CoursesForLecturer";
const BASED_DELETE_STUDENT="http://"+host+"/api/Users/DeleteStudent";
const BASED_DELETE_STUDENT_USER="http://"+host+"/api/Users/DeleteStudentUser";
const BASED_DELETE_LECTURER="http://"+host+"/api/Users/DeleteLecturer";
const BASED_DELETE_LECTURER_USER="http://"+host+"/api/Users/DeleteLecturerUser";
const BASED_DELETE_COURSE="http://"+host+"/api/Users/DeleteCourse";
const GET_ALL_USERS="http://"+host+"/api/Users/admin";
const POST_ICON="http://"+host+"/api/Users/UploadFiles";



export function postIcon(token, selectedImageFile) {
    const form = new FormData();
    form.append('file', selectedImageFile);
    return axios.post(POST_ICON,form,{headers: {Authorization: "Bearer "+token}}).then(response => { return response.data });
}

export function postLogIn(user) {
    return axios.post(LOGIN_URL,user).then(response => { return response.data });
}

export function postSignUp(user) {
    return axios.post(SIGNUP_URL,user).then(response => { return response.data });
}

export function getUserName(token) {
    return axios.get(GET_USERNAME_URL,{headers: {Authorization: "Bearer "+token}}).then(response => { return response.data });
}

export function addCourse(token,course) {
    return axios.post(POST_COURSE_URL,course,{headers: {Authorization: "Bearer "+token}}).then(response => { return response.data });
}

export function updateCourse(token,course,courseid) {
    return axios.put(`${UPDATE_COURSE_URL}/${courseid}`,course,{headers: {Authorization: "Bearer "+token}}).then(response => { return response.data });
}

export function enrollStudent(token,student) {
    return axios.post(POST_STUDENT_URL,student,{headers: {Authorization: "Bearer "+token}}).then(response => { return response.data });
}

export function enrollLecturer(token,lecturer) {
    return axios.post(POST_LECTURER_URL,lecturer,{headers: {Authorization: "Bearer "+token}}).then(response => { return response.data });
}

export function updateEmail(token,email) {
    return axios.post(POST_EMAIL_URL,email,{headers: {Authorization: "Bearer "+token}}).then(response => { return response.data });
}

export function getAllUsers(token) {
    return axios.get(GET_ALL_USERS,{headers: {Authorization: "Bearer "+token}}).then(response => { return response.data });
}

export function getAllCourse(token) {
    return axios.get(GET_ALL_COURSES,{headers: {Authorization: "Bearer "+token}}).then(response => { return response.data });
}

export function getAllStudents(token) {
    return axios.get(GET_ALL_STUDENTS,{headers: {Authorization: "Bearer "+token}}).then(response => { return response.data });
}

export function getAllLecturer(token) {
    return axios.get(GET_ALL_LECTURERS,{headers: {Authorization: "Bearer "+token}}).then(response => { return response.data });
}

export function studentEnrollCourse(token,studentid,courseid) {
    return axios.post(`${BASED_STUDENTCOURSE_URL}/${studentid}/${courseid}`,{},{headers: {Authorization: "Bearer "+token}}).then(response => { return response.data });
}

export function studentDropCourse(token,studentid,courseid) {
    return axios.delete(`${BASED_STUDENTCOURSE_URL}/${studentid}/${courseid}`,{headers: {Authorization: "Bearer "+token}}).then(response => { return response.data });
}


export function getStudentsForCourse(token,courseid) {
    return axios.get(`${GET_STUDENTS_FOR_COURSE}/${courseid}`,{headers: {Authorization: "Bearer "+token}}).then(response => { return response.data });
}

export function getCourseDetail(token,courseid) {
    return axios.get(`${GET_COURSE_DETAIL}/${courseid}`,{headers: {Authorization: "Bearer "+token}}).then(response => { return response.data });
}

export function getLecturersForCourse(token,courseid) {
    return axios.get(`${GET_LECTURERS_FOR_COURSE}/${courseid}`,{headers: {Authorization: "Bearer "+token}}).then(response => { return response.data });
}

export function lecturerEnrollCourse(token,lecturerid,courseid) {
    return axios.post(`${BASED_LECTURERCOURSE_URL}/${lecturerid}/${courseid}`,{},{headers: {Authorization: "Bearer "+token}}).then(response => { return response.data });
}

export function lecturerDropCourse(token,lecturerid,courseid) {
    return axios.delete(`${BASED_LECTURERCOURSE_URL}/${lecturerid}/${courseid}`,{headers: {Authorization: "Bearer "+token}}).then(response => { return response.data });
}


export function fetchCourseDetail(token,courseid) {
    return axios.all([getStudentsForCourse(token,courseid),getLecturersForCourse(token,courseid),getCourseDetail(token,courseid)]);
}

export function getCoursesForLecturer(token,lecturerid) {
    return axios.get(`${GET_COURSES_FOR_LECTURER}/${lecturerid}`,{headers: {Authorization: "Bearer "+token}}).then(response => { return response.data });
}

export function deleteStudent(token,studentid) {
    return axios.delete(`${BASED_DELETE_STUDENT}/${studentid}`,{headers: {Authorization: "Bearer "+token}}).then(response => { return response.data });
}

export function deleteStudentUser(token,userid) {
    return axios.delete(`${BASED_DELETE_STUDENT_USER}/${userid}`,{headers: {Authorization: "Bearer "+token}}).then(response => { return response.data });
}

export function deleteLecturer(token,lecturerid) {
    return axios.delete(`${BASED_DELETE_LECTURER}/${lecturerid}`,{headers: {Authorization: "Bearer "+token}}).then(response => { return response.data });
}

export function deleteLecturerUser(token,userid) {
    return axios.delete(`${BASED_DELETE_LECTURER_USER}/${userid}`,{headers: {Authorization: "Bearer "+token}}).then(response => { return response.data });
}

export function deleteCourse(token,courseid) {
    return axios.delete(`${BASED_DELETE_COURSE}/${courseid}`,{headers: {Authorization: "Bearer "+token}}).then(response => { return response.data });
}


// axios.interceptors.response.use(undefined, err => {
//     const error = err.response;
//     // if error is 401 
//     if (error.status===401) {
//         console.log('401')
// } })
import React from 'react';

export default function Footer() {
    return(
        <footer className="footer-container">
            <p className="text-center"><small>&copy; 2018 All rights reserved. Designed by YuzeMa.</small></p>
        </footer>
    );
}
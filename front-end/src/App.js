import React, { Component } from 'react';
import {BrowserRouter,Route,Switch} from 'react-router-dom';
import {getUserName} from './API/LMS';

import './App.css';

import LogInPrompt from './Home/LogInPrompt';
import Footer from './Footer';
import TimeOut from './TimeOut';
import Message from './Home/Message';
import UnauthHome from './Home/UnauthHome/UnauthHome';
import AuthedHome from './Home/AuthedHome/AuthedHome';


class App extends Component {
  constructor(){
    super();
    this.state={
      userInfo: {},
      isAuthed: false,
      statusChanged: {value: false, message: ""}
    }
  }

  authChecked(data){
      this.setState({
        userInfo: data,
        isAuthed: true
    })
  }

  componentDidMount(){
    if(sessionStorage.getItem('key') !== null){
      getUserName(sessionStorage.getItem('key')).then(data=>this.authChecked(data)).catch(function (error) {
        console.log("Error Message : "+error);
      });
    }
  }

  onRefresh(){
    if(sessionStorage.getItem('key') !== null){
      getUserName(sessionStorage.getItem('key')).then(data=>this.authChecked(data)).catch(function (error) {
        console.log(error);
      });
    }
  }

  successLogIn(data){
    this.setState({
      userInfo: data,
      isAuthed: true,
      statusChanged: {value: true, message: "Welcome Back "+data.Name}
    })
  }

  onSuccessLogIn(){
    if(sessionStorage.getItem('key') !== null ){
      getUserName(sessionStorage.getItem('key')).then(data=>this.successLogIn(data)).catch(function (error) {
        console.log(error);
      });
    }
  }

  onLogOut(){
    this.setState({
      isAuthed:false,
      userInfo: {},
      statusChanged: {value: true, message: "Logout Success"}
    });
  }

  onTimeOut(){
    this.setState({
      userInfo: {},
      isAuthed:false
    });
  }

  onStatusChanged(){
    this.setState({statusChanged: {value: false, message: ""}})
  }

  render() {
    return (
      <main>
        {
          this.state.statusChanged.value ? 
          <Message switchStatus={this.onStatusChanged.bind(this)} message={this.state.statusChanged.message}/> 
          : 
          ''
        }
        <BrowserRouter>
            <Switch>
              {this.state.isAuthed ?
              <Route path="/" render={()=><AuthedHome refreshInfo={this.onRefresh.bind(this)} userInfo={this.state.userInfo} switchToUnauth={this.onLogOut.bind(this)}/>}/>
              :
              <Route exact path="/" render={()=><UnauthHome successLogIn={this.onSuccessLogIn.bind(this)}/>}/>
              }
              <Route exact path="/timeOut" render={()=><TimeOut switchToUnauth={this.onTimeOut.bind(this)}/>}/>
              <Route render={()=><LogInPrompt/>} />
            </Switch>
        </BrowserRouter>
        <Footer>
        </Footer> 
      </main>    
    );
  }
}

export default App;


// http response 拦截器
// axios.interceptors.response.use(
//   response => {
//       return response;
//   },
//   error => {
//       if (error.response) {
//           switch (error.response.status) {
//               case 401:
//                //这里跳转登陆
//                .....
//               }
//   });
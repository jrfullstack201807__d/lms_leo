import React,{Component} from 'react';
import './EnrolledDetail.css';
import UploadIcon from './UploadIcon'
import {withRouter} from 'react-router-dom';
import {updateEmail,enrollLecturer,enrollStudent,getUserName} from '../API/LMS'

class EnrolledDetail extends Component{
    
    userType=this.props.userInfo.UserType;
    iconURL=this.props.userInfo.IconURL;
    token = sessionStorage.getItem('key');

    constructor(props){
        super(props);
        this.state={
            message: "",
            name: {value: "",isValid: true},
            email: {value: "",isValid: true},
            submit: "submit"
        }
    }

    onNameChange(e){
        this.setState({name: {value:e.target.value, isValid:true}})
    }

    onEmailChange(e){
        this.setState({email: {value:e.target.value, isValid:true}})
    }

    onNameSubmit(e){
        e.preventDefault();
        this.setState({
            submit: "button"
        })
        if(this.nameValidation()){
            let user = {
                Id: 0, 
                FullName:this.state.name.value,
                UserId: this.props.userInfo.Id
            }
            if(this.userType==="Student"){
                enrollStudent(this.token,user).then(data=>this.onRefreshPage(data)).catch(function (error) {
                    console.log(error);
                  });
            }else if(this.userType==="Lecturer"){
                enrollLecturer(this.token,user).then(data=>this.onRefreshPage(data)).catch(function (error) {
                    console.log(error);
                  });
            }else{
                console.log('error');
            }
            this.setState({
                name: {value:"",isValid:true},
                submit: "submit"
            })
        }else{
            this.setState({
                submit: "submit"
            })
        }
    }

    onRefreshPage(data){
        console.log(data);
        this.props.refreshInfo();
    }

    onEmailSubmit(e){
        e.preventDefault();
        this.setState({
            submit: "button"
        })
        let email ={Email:this.state.email.value}
        if(this.emailValidation()){
            updateEmail(this.token,email).then(data=>alert(data)).catch(function (error) {
                console.log(error);
              });
            console.log(this.state.email.value);
            this.setState({
                email: {value:"",isValid:true},
                submit: "submit"
            })
        }else{
            this.setState({
                submit: "submit"
            })
        }
    }

    getUserName(){
        getUserName(this.token).then(data=>this.handleUserName(data)).catch(function (error) {
            console.log(error);
          });
    }

    handleUserName(data){
        this.setState({
            name: {value: data.Name,isValid: true},
        })
    }

    nameValidation(){
        let isValid=true;
        let i = /[0-9]+/;
        let spec=/[,.<>{}~!@#$%^&*_]/;
        let name=this.state.name.value;
        if(i.test(name)||spec.test(name)||name === ""){
            this.setState({name:{value: name,isValid:false}});
            isValid=false;
        }
        
        return isValid;
    }

    emailValidation(){
        let isValid=true;
        let email=this.state.email.value;
        let filter=/^[A-Za-zd]+([-_.][A-Za-zd]+)*@([A-Za-zd]+[-.])+[A-Za-zd]{2,5}$ /;
        if(filter.test(email)||email === ""){
            this.setState({email:{email: email,isValid:false}});
            isValid=false;
        }
        return isValid;
    }

    messageSwitch(){
        this.setState({
            message: '',
        })
    }

    handleUploadResult(res){
        this.setState({
            message: res.Message
        })
    }

    render(){
        return(
            <div>
                {
                    this.state.message === '' ?
                     ''
                    :
                    <div className="message-box">
                        <p>{this.state.message}     <i className="far fa-times-circle" onClick={this.messageSwitch.bind(this)}/></p> 
                    </div>
                }
                <UploadIcon handleUploadResult={this.handleUploadResult.bind(this)}/>
                
                <div className="enrolled-detail container rounded shadow">
                <h4 className="text-center secondary-header">Personal Detail</h4>
                <div className="row"><label className="col text-center currentIcon">Current Icon : </label>    <div className="col"><img className="personal-icon" src={this.iconURL} alt="current-icon"/></div></div>
                
                    <div className="row">
                    <label className="col text-center">Full Name : </label>
                {
                    this.props.userInfo.IsEnrolled || this.userType==="Admin" ?
                    <div className="col">{this.userType==="Admin" ? <span> {this.props.userInfo.Name}</span>:<span> {this.props.userInfo.FullName}</span>}</div>
                    
                    :
                    <form className="col" onSubmit={this.onNameSubmit.bind(this)}>
                        <input className="ml-1" type="text" name="name" value={this.state.name.value} placeholder="Full Name" onChange={this.onNameChange.bind(this)}/>
                        {this.state.name.isValid ? '':<p>Error! Name only contain letters.</p>}
                        <button className="btn btn-warning ml-1" type="button" onClick={this.getUserName.bind(this)}>As UserName</button>
                        <button className="btn btn-primary ml-1" type={this.state.submit}>Submit</button>
                    </form>
                }
                </div>
                
                <div className="row">
                    <label className="col text-center">Email Address : </label>
                    {  
                    this.props.userInfo.EmailConfirmed ?
                    <span className="col">  {this.props.userInfo.Email}</span>
                    :
                    <form className="col" onSubmit={this.onEmailSubmit.bind(this)}>
                        <input className="ml-1" type="text" name="email" value={this.state.email.value} placeholder="Email Address" onChange={this.onEmailChange.bind(this)}/>
                        {this.state.email.isValid ? '':<p>Error! Incorrect Format.</p>}
                        <button className="btn btn-primary ml-1" type={this.state.submit}>Submit</button>
                    </form>
                }
                </div>
                <div className="row"><label className="col text-center">UserType : </label><span className="col">  {this.props.userInfo.UserType}</span></div>
                </div>
            </div>
        );  
    }
}

export default withRouter(EnrolledDetail);
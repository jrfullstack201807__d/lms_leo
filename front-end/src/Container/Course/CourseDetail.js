import React,{Component} from 'react';
import {updateCourse} from '../../API/LMS'

export default class CourseDetail extends Component {
    constructor(props){
        super(props);
        this.state={
            credit: this.props.courseDetail[2].Credit,
            courseDescription: this.props.courseDetail[2].Description,
            isEditing: false,
            buttonValue: "Edit",
            message: ''
        }
    }

    courseid = this.props.courseDetail[2].Id;
    token = sessionStorage.getItem('key');

    onCreditChange(e) {
        this.setState({
            credit: e.target.value
        })
    }

    onDescrptionChange(e) {
        this.setState({
            courseDescription: e.target.value
        })
    }

    onEdit() {
        if(this.state.isEditing){
            if(this.validationCheck()){
                this.updateCourse();
            }
        }else{
            this.setState({
                isEditing: true,
                buttonValue: "Save"
            })
            this.props.onEditing();
        }
    }

    cancelEdit() {
        this.setState({
            buttonValue: "Edit",
            isEditing: false
        })
        this.props.onEditing();
    }

    updateCourse(){
        let course = {
            Credit: parseInt(this.state.credit),
            Description: this.state.courseDescription
        }
        updateCourse(this.token,course,this.courseid).then(data=>this.handleUpdateResult(data)).catch(function (error) {
            console.log(error);
          });
    }

    handleUpdateResult(data){
        if(data.Status === 1){
        this.setState({
            buttonValue: "Edit",
            message: data.Message,
            isEditing: false
        })
        this.props.onEditing();
        this.props.onRefresh(this.courseid);
        }else{
            console.log(data.Message)
        }
    }


    validationCheck() {
        let isValid = false;
        let credit = this.state.credit;
        console.log(credit);
        if( credit === '0' || credit === '1' || credit === '2' || credit === '3' ){
            isValid = true;
            return isValid;
        }
        this.setState({
            message: 'Unsuccess, Error Input !'
        })
        return isValid;
    }
    
    messageSwitch() {
        this.setState({
            message: ''
        })
    }

    render(){
        const isEditing=this.state.isEditing
        const course=this.props.courseDetail[2];
        

        
        const lecturersList = this.props.courseDetail[1].map(lecturer=>{
            return <DisplayFullName  detail={lecturer.Lecturer} key={lecturer.LecturerId} />
        })
        const studentsList = this.props.courseDetail[0].map(student=>{
            return <DisplayFullName  detail={student.Student} key={student.StudentId} />
        })


        return (
            <div className="contianer bg-img-container">
                {this.state.message==='' ? 
                ''
                :
                <div className="message-box">
                    <p>{this.state.message}      <i className="far fa-times-circle" onClick={this.messageSwitch.bind(this)}/></p>
                    
                </div>}
                <div className="row">
                    <h2 className="highlight-header col text-center"><b className="text-dark">Course: </b> {this.props.onClickCourse}</h2>
                    <div className="col-5 text-right">
                    <button className="btn btn-primary" onClick={this.onEdit.bind(this)}>{this.state.buttonValue}</button>
                    {this.state.isEditing?<button className="btn btn-secondary ml-1" onClick={this.cancelEdit.bind(this)}>cancel</button>:''}
                    </div>
                </div>
                <div>

                    <h4 className="secondary-header">Credit :</h4>
                    {
                        isEditing ? 
                        <div><input value={this.state.credit} onChange={this.onCreditChange.bind(this)} />Available From 0 To 3</div>
                        :
                        <b className="pl-5">{course.Credit}</b>
                    }

                    <h4 className="secondary-header">Description :</h4>
                    {
                        isEditing ? 
                        <textarea className="w-100" value={this.state.courseDescription} onChange={this.onDescrptionChange.bind(this)} />
                        :
                        <b className="pl-5">{course.Description}</b>
                    }


                    <h4 className="secondary-header">Lecturers :</h4>
                    { 
                        lecturersList.length > 0 ?
                        <ul>{lecturersList}</ul>
                        :
                        <b className="text-center pl-5">Lecturer Unassigned</b>
                    }
                    
                    <h4 className="secondary-header">Students :</h4>
                    {
                        this.props.displayStudentsList ? 
                        <ul>{studentsList}</ul>
                        :
                        <b className="text-center pl-5">Student List Only Available For Assigned Lecturer.</b>
                    }

                </div>
            </div>
        )
    }
}


function DisplayFullName(props){
    return(
        <li>
            <img className="personal-icon" src={props.detail.IconURL} alt="icon" />
            {props.detail.FullName}
        </li>
    )
}




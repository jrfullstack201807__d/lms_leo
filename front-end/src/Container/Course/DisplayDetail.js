import React,{Component} from 'react';

export default class DisplayDetail extends Component{
    constructor(props){
        super(props)
        this.state={
            newStudent: "",
            newLecturer: "",
            submit: "submit",
            studentMessage: "",
            lecturerMessage:"",
            addStudentConfirm: false,
            addLecturerConfirm: false
        }
    }

    users=this.props.allUsers;

    studentUsers = this.users.filter(user=>{
        return user.Student !== undefined
    })

    lecturerUsers = this.users.filter(user=>{
        return user.Lecturer !== undefined
    })

    onNewStudent(e){
        this.setState({
            newStudent: e.target.value
        })
    }

    onNewLecturer(e){
        this.setState({
            newLecturer: e.target.value
        })
    }


    handleEnrollStudent(e){
        e.preventDefault();
        this.setState({submit:"button"})
        if(!this.isStudentExist()){
            this.setState({
                studentMessage: "student: "+ this.state.newStudent+" is not a enrolled student",
                submit:"submit",
                newStudent: ""
            })
        }else if(this.isStudentEnrolled()){
            this.setState({
                studentMessage: "student "+this.state.newStudent+" is already enrolled in this course",
                submit:"submit",
                newStudent: ""
            })
        }else{
            this.setState({
                addStudentConfirm: true
            })
        }
    }

    isStudentEnrolled(){
        return this.props.studentsInfo.some(student=>student.Student.FullName.toLowerCase() === this.state.newStudent.toLowerCase() || student.Student.Id === parseInt(this.state.newStudent))
    }
    isStudentExist(){
        return this.studentUsers.some(user=> user.Student.FullName.toLowerCase() === this.state.newStudent.toLowerCase() || user.Student.Id === parseInt(this.state.newStudent))
    }

    isLecturerEnrolled(){
        return this.props.lecturersInfo.some(lecturer=>lecturer.Lecturer.FullName.toLowerCase() === this.state.newLecturer.toLowerCase() || lecturer.Lecturer.Id === parseInt(this.state.newLecturer))
    }
    
    isLecturerExist(){
        return this.lecturerUsers.some(user=> user.Lecturer.FullName.toLowerCase() === this.state.newLecturer.toLowerCase() || user.Lecturer.Id === parseInt(this.state.newLecturer))
    }



    handleEnrollLecturer(e){
        e.preventDefault();
        this.setState({submit:"button"})
        //find  lecturer
        if(!this.isLecturerExist()){
            this.setState({
                lecturerMessage: "lecturer: "+ this.state.newLecturer+" is not a enrolled lecturer",
                submit:"submit",
                newLecturer: ""
            })
        }else if(this.isLecturerEnrolled()){
            this.setState({
                lecturerMessage: "lecturer "+this.state.newLecturer+" is already enrolled in this course",
                submit:"submit",
                newLecturer: ""
            })
        }else{
            this.setState({
                addLecturerConfirm: true
            })
        }
    }

    enrollStudent(){
        let isId=!isNaN(this.state.newStudent);
        let selectedId = -1;
        if(isId){
            selectedId = parseInt(this.state.newStudent)
        }else{
            let selectedStudent = this.studentUsers.find(student=>{
                return student.Student.FullName === this.state.newStudent
            })
            selectedId = selectedStudent.Student.Id;
        }
        console.log(selectedId)
        this.props.enrollStudent(selectedId);
        this.setState({
            submit:"submit",
            newStudent: "",
            addStudentConfirm: false
        })
    }
    
    enrollLecturer(){
        let isId=!isNaN(this.state.newLecturer);
        let selectedId = -1;
        if(isId){
            selectedId = parseInt(this.state.newLecturer)
        }else{
            let selectedLecturer = this.lecturerUsers.find(lecturer=>{
                return lecturer.Lecturer.FullName === this.state.newLecturer
            })
            selectedId = selectedLecturer.Lecturer.Id;
        }
        console.log(selectedId)
        this.props.enrollLecturer(selectedId);
        this.setState({
            submit:"submit",
            newLecturer: "",
            addLecturerConfirm: false
        })
    }

    closeMessage(){
        this.setState({
            studentMessage: "",
            lecturerMessage: ""
        })
    }

    cancelEnroll(){
        this.setState({
            addStudentConfirm: false,
            addLecturerConfirm: false
        })
    }

    render(){
        return(
            <div className="row">
                <div className="rounded shadow bg-img-container col">
                    <form onSubmit={this.handleEnrollStudent.bind(this)}>
                        <label>Enroll New Student : </label>
                        <input value={this.state.newStudent} placeholder="Student Name/Id" onChange={this.onNewStudent.bind(this)}/>
                        <button className="more-button" type={this.state.submit}>Search</button>
                    </form>
                    {
                        this.state.addStudentConfirm ?
                        <div className="message-box">Would like to add student: {this.state.newStudent} into the course? <button className="add-button" onClick={this.enrollStudent.bind(this)}>Confirm</button><button className="delete-button" onClick={this.cancelEnroll.bind(this)}>Cancel</button></div>
                        :
                        ''
                    }
                    {
                        this.state.studentMessage === "" ?
                        ''
                        :
                        <p className="message-box">{this.state.studentMessage}<i className="far fa-times-circle" onClick={this.closeMessage.bind(this)}/></p>
                    }
                    <p><b>Students List: </b></p>
                    {
                        this.props.studentsInfo.length > 0 ?
                        <StudentsList enrolledInfo={this.props.studentsInfo} onRemove={this.props.onRemoveStudent}/>
                        :
                        <p className="text-center"><b>Students Empty</b></p>
                     }
                    
                </div>
                <div className="rounded shadow bg-img-container col">
                    <form onSubmit={this.handleEnrollLecturer.bind(this)}>
                        <label>Enroll New Lecturer : </label>
                        <input value={this.state.newLecturer} placeholder="Lecturer Name/Id" onChange={this.onNewLecturer.bind(this)}/>
                        <button className="more-button" type={this.state.submit}>Search</button>
                    </form>
                    {
                        this.state.addLecturerConfirm ?
                        <div className="message-box">Would like to add lecturer: {this.state.newLecturer} into the course? <button className="add-button" onClick={this.enrollLecturer.bind(this)}>Confirm</button><button className="delete-button" onClick={this.cancelEnroll.bind(this)}>Cancel</button></div>
                        :
                        ''
                    }
                    {
                        this.state.lecturerMessage === "" ?
                        ''
                        :
                        <p className="message-box">{this.state.lecturerMessage}<i className="far fa-times-circle" onClick={this.closeMessage.bind(this)}/></p>
                    }
                    <p><b>Lecturers List: </b></p>
                    {
                        this.props.lecturersInfo.length > 0 ?
                        <LecturersList enrolledInfo={this.props.lecturersInfo} onRemove={this.props.onRemoveLecturer}/>
                        :
                        <p className="text-center"><b>Lecturers Unassigned</b></p>
                    }
                </div>
            </div>
        )
    }
}

function StudentsList (props){
    let rows = []
    rows = props.enrolledInfo.map(row=>{
        return <Student enrolledInfo={row} key={row.StudentId} onRemove={props.onRemove}/>
    })
    return(
        <ul>{rows}</ul>
    )
}

function Student (props){
    let FullName = props.enrolledInfo.Student.FullName;
    let StudentId = props.enrolledInfo.Student.Id;
    let CourseId = props.enrolledInfo.CourseId;
    let IconURL = props.enrolledInfo.Student.IconURL;
    return(
        <li className="container detail-box">
             <div className="row">
                <p className="col"><img className="personal-icon" src={IconURL} alt="icon" />{FullName}</p>
                <div className="col"><button className="delete-button adjust-delete-button" onClick={()=>props.onRemove(StudentId,CourseId)}>Remove</button></div>
             </div>
        </li>
    )
}

function LecturersList (props){
    let rows = []
    rows = props.enrolledInfo.map(row=>{
        return <Lecturer enrolledInfo={row} key={row.LecturerId} onRemove={props.onRemove}/>
    })
    return(
        <ul>{rows}</ul>
    )
}

function Lecturer (props){
    let FullName = props.enrolledInfo.Lecturer.FullName;
    let LecturerId = props.enrolledInfo.Lecturer.Id;
    let CourseId = props.enrolledInfo.CourseId;
    let IconURL = props.enrolledInfo.Lecturer.IconURL;
    return(
        <li className="container detail-box">
            <div className="row">
                <p className="col"><img className="personal-icon" src={IconURL} alt="icon" />{FullName}</p>
                <div className="col"><button className="delete-button adjust-delete-button" onClick={()=>props.onRemove(LecturerId,CourseId)}>Remove</button></div>
            </div>
        </li>
    )
}
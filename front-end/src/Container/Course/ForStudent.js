import React,{Component} from 'react';
import '../Container.css'
import {getAllCourse,fetchCourseDetail} from '../../API/LMS'
export default class ForStudent extends Component{
    constructor(props){
        super(props);
        this.state={
            displayStudentsList: false,
            isLoading: true,
            enrolledCourses: [],
            allCourses: [],
            onClickCourse: "",
            courseDetail: [],
            searchResultMessage: "",
            onSearch: "",
            needClearSearch: false
        }
    }

    studentId = this.props.userInfo.EnrolledId;
    token = sessionStorage.getItem('key');

    componentDidMount(){
        this.getCourses();
    }

    getCourses(){
        getAllCourse(this.token).then(data=>this.arrayFilter(data)).catch(function (error) {
            console.log(error);
          });
    }

    isEnrolled(courses){
        return   courses.some( item => item.StudentId === this.studentId ) 
    }

    arrayFilter(data){
        let enrolledArray= [];
        //console.log(data);

        enrolledArray = data.filter(course=>{
            return  this.isEnrolled(course.StudentsCourses)})
        //console.log(enrolledArray);
        this.setState({
            isLoading: false,
            enrolledCourses: enrolledArray,
            allCourses: data
        })
    }

    loadDetail(courseId){
        fetchCourseDetail(this.token,courseId).then(data=>this.handleCourseDetail(data,courseId)).catch(function (error) {
            console.log(error);
        })
    }

    handleCourseDetail(data,courseId){
        //console.log(data)
        let displayStudentsList = false;
        this.state.enrolledCourses.forEach(item=>{
            if(item.Id === courseId){
                displayStudentsList = true
            }
        })
        
        let course = this.state.allCourses.find(function(course) {
            return course.Id === courseId
        })
        this.setState({
            displayStudentsList: displayStudentsList,
            onClickCourse: course.CourseName,
            courseDetail: data
        })
    }

    handleOnSearch(e){
        e.preventDefault();
        const selectedCourse = this.state.allCourses.find(course=>{
            return course.CourseName.toLowerCase() === this.state.onSearch.toLowerCase() || course.Id === parseInt(this.state.onSearch)
        });
        if(selectedCourse !== undefined){
            this.loadDetail(selectedCourse.Id);
            this.setState({
                needClearSearch: true,
                searchResultMessage: ""
            })
        }else{
            this.setState({
                onSearch: "",
                searchResultMessage: "Course Name or Course Id does not exist"
            })
        }
    }

    searchChanged(e){
        this.setState({
            onSearch: e.target.value
        })
    }

    clearSearch(){
        this.setState({
            onClickCourse: "",
            onSearch: "",
            needClearSearch: false
        })
    }

    closeSearchMessage() {
        this.setState({
            searchResultMessage: ""
        })
    }


    render(){
        return(
            <div className="container">
                {
                    this.state.isLoading ?
                    <p>Loading...</p>
                    :
                    <div>
                        <div>
                            <form  onSubmit={this.handleOnSearch.bind(this)}>
                                <label>Search Course : </label>
                                <input value={this.state.onSearch} placeholder="Course Name/Id" onChange={this.searchChanged.bind(this)} />
                                {
                                    this.state.needClearSearch ?
                                    <div className="clear-search" onClick={this.clearSearch.bind(this)}>Clear Search</div>
                                    :
                                    <button className="more-button ml-2" type="submit" >Search</button>
                                }
                            </form>
                            {
                                this.state.searchResultMessage === "" ?
                                ''
                                :
                                <p className="message-box">{this.state.searchResultMessage}<i className="far fa-times-circle" onClick={this.closeSearchMessage.bind(this)}/></p>
                            }
                        </div>
                        <div className="row">
                            <div className="col-lg adjust-container">
                                <div className="highlight-header">Enrolled Courses :</div>
                                <DisplayCourses courses={this.state.enrolledCourses} loadDetail={this.loadDetail.bind(this)}/>
                                <div className="highlight-header">All Courses List :</div>
                                <DisplayCourses courses={this.state.allCourses} loadDetail={this.loadDetail.bind(this)}/>
                            </div>
                            <div className="col-lg">
                            { 
                                this.state.onClickCourse==="" ?
                                <div className="text-center text-primary adjust-container">
                                    <div className="see-detail">Click More Button To See Detail</div>
                                    <img className="course-image" src="https://img.freepik.com/free-vector/teacher-with-a-blackboard-design_1214-224.jpg?size=338&ext=jpg" alt="course" />
                                </div>
                                :
                                <div className="bg-img-container">
                                    <h2 className="highlight-header">{this.state.onClickCourse}</h2>
                                    <DisplayDetail courseDetail={this.state.courseDetail} displayStudentsList={this.state.displayStudentsList}/>
                                </div>
                            }
                            </div>
                        </div>
                    </div>
                }
            </div>
        );  
    }
}


function DisplayDetail(props){
    let students=props.courseDetail[0];
    let lecturers=props.courseDetail[1];
    let course=props.courseDetail[2];
    let lecturersList = [];
    let studentsList = [];
    lecturersList = lecturers.map(lecturer=>{
        return <DisplayFullName  detail={lecturer.Lecturer} key={lecturer.LecturerId} />
    })
    studentsList = students.map(student=>{
        return <DisplayFullName  detail={student.Student} key={student.StudentId} />
    })

    return  <div className="detail-box">
                <h3 className="secondary-header">Credit :</h3>
                {course.Credit}

                <h3 className="secondary-header">Description :</h3>
                {course.Description}
                <h3 className="secondary-header">Lecturers :</h3>
                { 
                    lecturersList.length > 0 ?
                    <ul>{lecturersList}</ul>
                    :
                    <p>Lecturer Unassigned</p>
                }
                
                <h3 className="secondary-header">Students :</h3>
                {
                    props.displayStudentsList ? 
                    <ul>{studentsList}</ul>
                    :
                    <h4>You are not in this course, Only Available For Enrolled Student.</h4>
                }

            </div>
}

function DisplayFullName(props){
    return(
        <li>
            <img className="personal-icon" src={props.detail.IconURL} alt="icon" />
            {props.detail.FullName}
        </li>
    )
}


function DisplayCourses(props){
    let rows = [];
    rows = props.courses.map(course=>{
        return <Course courseName={course.CourseName} loadDetail={props.loadDetail} id={course.Id} key={course.Id} />
    })
    return <div className="pr-3"><ul>{rows}</ul></div>
}


function Course(props){
    return(
        <li className="detail-item">
            <p className="space-between-container">
                <span><b>Course Name:</b> {props.courseName}</span>
                <span><b>Course Id:</b> {props.id} </span>
            </p>
            <button className="more-button" onClick={()=>props.loadDetail(props.id)}>More</button>
        </li>
    )
}

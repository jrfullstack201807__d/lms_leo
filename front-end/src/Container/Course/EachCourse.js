import React,{Component} from 'react';
import DisplayDetail from './DisplayDetail'
import { fetchCourseDetail,studentDropCourse,lecturerDropCourse, studentEnrollCourse,lecturerEnrollCourse } from '../../API/LMS';

export default class EachCourse extends Component{
    constructor(props){
        super(props);
        this.state={
            courseDetailSwitch: false,
            deleteSwitch: false,
            displayDetail: false,
            enrolledStudents: [],
            enrolledLecturers: []
        }
    }

    courseId=this.props.courseDetail.Id;
    token = sessionStorage.getItem('key');

    componentDidMount(){
        this.loadDetail();
    }

    loadDetail(){
        fetchCourseDetail(this.token,this.courseId).then(data=>this.handleFetchData(data)).catch(function (error) {
            console.log(error);
          });
    }

    handleFetchData(data){
        // console.log(data)
        this.setState({
            enrolledStudents: data[0],
            enrolledLecturers: data[1]
        })
    }

    deleteSwitch(){
        this.setState({
            deleteSwitch: !this.state.deleteSwitch
        })
    }

    displayDetailSwitch(){
        this.setState({
            displayDetail: !this.state.displayDetail
        })
    }

    refreshData(data){
        console.log(data);
        this.loadDetail();
    }

    onRemoveStudent(StudentId,CourseId){
        studentDropCourse(this.token,StudentId,CourseId).then(data=>this.refreshData(data)).catch(function (error) {
            try {
                if(error.response.status===404){
                alert('Error, Already been updated by another user. Please Refresh Page')
                }
            }catch(e) {
            }finally {
                console.log(error);
            }
          });
    }
    onRemoveLecturer(LecturerId,CourseId){
        lecturerDropCourse(this.token,LecturerId,CourseId).then(data=>this.refreshData(data)).catch(function (error) {
            try {
                if(error.response.status===404){
                alert('Error, Already been updated by another user. Please Refresh Page')
                }
            }catch(e) {
            }finally {
                console.log(error);
            }
          });
    }

    enrollStudent(studentId){
        studentEnrollCourse(this.token,studentId,this.courseId).then(data=>this.refreshData(data)).catch(function (error) {
            try {
                if(error.response.status===404){
                alert('Error, Already been updated by another user. Please Refresh Page')
                }
            }catch(e) {
            }finally {
                console.log(error);
            }
          });
    }

    enrollLecturer(lecturerId){
        lecturerEnrollCourse(this.token,lecturerId,this.courseId).then(data=>this.refreshData(data)).catch(function (error) {
            try {
                if(error.response.status===404){
                alert('Error, Already been updated by another user. Please Refresh Page')
                }
            }catch(e) {
            }finally {
                console.log(error);
            }
          });
    }
    render(){
        return(
            <li className="detail-item container">
            <p onClick={this.displayDetailSwitch.bind(this)}>{this.props.courseDetail.CourseName}</p>
            {
                this.state.deleteSwitch ?
                <div>
                    <p className="message-box">Delete Course Could Affect On Other Users</p>
                    <button className="delete-button" onClick={()=>this.props.onDeleteCourse(this.courseId)}>Confirm Delete</button><button className="add-button" onClick={this.deleteSwitch.bind(this)}>Cancel</button>
                </div>
                :
                <button className="delete-secondary-button" onClick={this.deleteSwitch.bind(this)}>Delete</button>
            }
            {
                this.state.displayDetail ? 
                <DisplayDetail studentsInfo={this.state.enrolledStudents} 
                    lecturersInfo={this.state.enrolledLecturers} 
                    onRemoveStudent={this.onRemoveStudent.bind(this)} 
                    onRemoveLecturer={this.onRemoveLecturer.bind(this)}
                    enrollStudent={this.enrollStudent.bind(this)}
                    enrollLecturer={this.enrollLecturer.bind(this)}
                    allUsers={this.props.allUsers}/>
                :
                ''
            }
            
            </li>
        )
    }
}
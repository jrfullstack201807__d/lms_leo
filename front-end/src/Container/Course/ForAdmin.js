import React,{Component} from 'react';
import EachCourse from './EachCourse'
import { getAllCourse,addCourse,deleteCourse,getAllUsers } from '../../API/LMS';

export default class ForAdmin extends Component{
    constructor(props){
        super(props);
        this.state={
            newCourse: "",
            courseCredit: 0,
            courseDescription: "",
            message: "",
            displayMessage: false,
            allCourses: [],
            submit: "submit",
            allUsers: [],
            searchResultMessage: "",
            onSearch: "",
            needClearSearch: false
        }
    }

    token = sessionStorage.getItem('key');

    componentDidMount(){
        this.getAllCourses();
        this.loadAllUsers();
    }

    getAllCourses(){
        getAllCourse(this.token).then(data=>this.handleCoursesData(data)).catch(function (error) {
            console.log(error);
          });
    }

    handleCoursesData(data){
        // console.log(data)
        this.setState({
            allCourses: data
        })
    }

    loadAllUsers(){
        getAllUsers(this.token).then(data=>this.handleUsersData(data)).catch(function (error) {
            console.log(error);
          });
    }

    handleUsersData(data){
        // console.log(data)
        this.setState({
            allUsers: data
        })
    }

    onAddNewCourse(e){
        this.setState({
            newCourse: e.target.value
        })
    }

    handleSubmit(e){
        e.preventDefault();
        this.setState({submit:"button"})
        let course = {
            Id: 0,
            Credit: this.state.courseCredit,
            CourseName: this.state.newCourse,
            Description: this.state.courseDescription
        }
        addCourse(this.token,course).then(data=>this.handleNewCourse(data)).catch(function (error) {
            console.log(error);
        });
        this.setState({
            submit:"submit",
            newCourse: "",
            courseDescription: ""
        })
    }

    handleNewCourse(data){
        if(data.Status===1){
            this.getAllCourses();
        }
        this.setState({
            message: data.Message,
            displayMessage: true,
        })
    }

    messageSwitch(){
        this.setState({
            displayMessage: false,
        })
    }

    onDeleteCourse(courseId){
        deleteCourse(this.token,courseId).then(data=>this.handleNewCourse(data)).catch(function (error) {
            console.log(error);
          });
    }

    onSelectChange(e){
        this.setState({courseCredit:e.target.value})
    }

    onDescrptionChange(e){
        this.setState({
            courseDescription: e.target.value
        })
    }

    handleOnSearch(e){
        e.preventDefault();
        const selectedCourse = this.state.allCourses.filter(course=>{
            return course.CourseName.toLowerCase() === this.state.onSearch.toLowerCase() || course.Id === parseInt(this.state.onSearch)
        });
        if(selectedCourse.length>0){
            this.setState({
                allCourses: selectedCourse,
                needClearSearch: true,
                searchResultMessage: ""
            })
        }else{
            this.setState({
                onSearch: "",
                searchResultMessage: "Course Name or Course Id does not exist"
            })
        }
    }

    searchChanged(e){
        this.setState({
            onSearch: e.target.value
        })
    }

    clearSearch(){
        this.setState({
            onSearch: "",
            needClearSearch: false
        })
        this.getAllCourses();
        this.loadAllUsers();
    }

    closeSearchMessage() {
        this.setState({
            searchResultMessage: ""
        })
    }

    render(){
        return(
            <div>
                <div>
                    <form  onSubmit={this.handleOnSearch.bind(this)}>
                        <label>Search Course : </label>
                        <input value={this.state.onSearch} placeholder="Course Name/Id" onChange={this.searchChanged.bind(this)} />
                        {
                            this.state.needClearSearch ?
                            <div className="clear-search" onClick={this.clearSearch.bind(this)}>Clear Search</div>
                            :
                            <button className="more-button ml-2" type="submit" >Search</button>
                        }
                    </form>
                    {
                        this.state.searchResultMessage === "" ?
                        ''
                        :
                        <p className="message-box">{this.state.searchResultMessage}<i className="far fa-times-circle" onClick={this.closeSearchMessage.bind(this)}/></p>
                    }
                </div>


                <form className="bg-img-container rounded shadow" onSubmit={this.handleSubmit.bind(this)}>
                    <h4 className="text-center text-primary">Add New Course</h4>
                    <div className="space-between-container">
                    <label>Enter New Course : </label>
                    <input value={this.state.newCourse} placeholder="Course Name" onChange={this.onAddNewCourse.bind(this)}/>
                    <label>Course Credit: </label>
                    <select onChange={this.onSelectChange.bind(this)}>
                        <option value="0">0</option>
                        <option value="1">1</option>
                        <option value="2">2</option>
                        <option value="3">3</option>
                    </select>
                    <label>Description : </label>
                    <textarea value={this.state.courseDescription} placeholder="Enter Course Description" onChange={this.onDescrptionChange.bind(this)} />
                    <button type={this.state.submit}>Submit</button>
                    </div>
                </form>
                
                {
                    this.state.displayMessage ?
                    <div className="message-box">
                        <p>{this.state.message}     <i className="far fa-times-circle" onClick={this.messageSwitch.bind(this)}/></p> 
                    </div>
                    :
                    ''
                }
                <AllCourses allCourses={this.state.allCourses} onDeleteCourse={this.onDeleteCourse.bind(this)} allUsers={this.state.allUsers}/>
            </div>
        ); 
    } 
}

function AllCourses(props){
    let coursesArray = [];
    coursesArray=props.allCourses.map(course=>{
        return <EachCourse courseDetail={course} key={course.Id} onDeleteCourse={props.onDeleteCourse} allUsers={props.allUsers}/>
    })
    return(
        <ul>{coursesArray}</ul>
    )
}

import React,{Component} from 'react';
import CourseDetail from './CourseDetail';
import {getAllCourse,fetchCourseDetail} from '../../API/LMS'
export default class ForLecturer extends Component{
    constructor(props){
        super(props);
        this.state={
            displayStudentsList: false,
            isEditing: false,
            isLoading: true,
            enrolledCourses: [],
            allCourses: [],
            onClickCourse: "",
            courseDetail: [],
            message: '',
            searchResultMessage: "",
            onSearch: "",
            needClearSearch: false
        }
    }
    lecturerId = this.props.userInfo.EnrolledId;
    token = sessionStorage.getItem('key');


    componentDidMount(){
        this.getCourses();
    }

    getCourses(){
        getAllCourse(this.token).then(data=>this.arrayFilter(data)).catch(function (error) {
            console.log(error);
          });
    }

    arrayFilter(data){
        let enrolledArray= [];
        // console.log(data);

        enrolledArray = data.filter(course=>{
            return  this.isEnrolled(course.LecturersCourses)
        })
        // console.log(enrolledArray);
        this.setState({
            isLoading: false,
            enrolledCourses: enrolledArray,
            allCourses: data
        })
    }

    isEnrolled(courses){
        return   courses.some( item => item.LecturerId === this.lecturerId ) 
    }


    loadDetail(courseId){
        if(this.state.isEditing){
            this.setState({
                message: 'Unable To Redirect, Please Save or Cancel Course Edition'
            })
        }else{
            fetchCourseDetail(this.token,courseId).then(data=>this.handleCourseDetail(data,courseId)).catch(function (error) {
                console.log(error);
            })
        }
    }

    handleCourseDetail(data,courseId){
        //console.log(data)
        let displayStudentsList = false;
        this.state.enrolledCourses.forEach(item=>{
            if(item.Id === courseId){
                displayStudentsList = true
            }
        })
        
        let course = this.state.allCourses.find(function(course) {
            return course.Id === courseId
        })

        this.setState({
            displayStudentsList: displayStudentsList,
            onClickCourse: course.CourseName,
            courseDetail: data
        })
    }

    onEditing() {
        this.setState({
            isEditing: !this.state.isEditing
        })
    }

    messageSwitch() {
        this.setState({
            message: ''
        })
    }

    handleOnSearch(e){
        e.preventDefault();
        const selectedCourse = this.state.allCourses.find(course=>{
            return course.CourseName.toLowerCase() === this.state.onSearch.toLowerCase() || course.Id === parseInt(this.state.onSearch)
        });
        if(selectedCourse !== undefined){
            this.loadDetail(selectedCourse.Id);
            this.setState({
                needClearSearch: true,
                searchResultMessage: ""
            })
        }else{
            this.setState({
                onSearch: "",
                searchResultMessage: "Course Name or Course Id does not exist"
            })
        }
    }

    searchChanged(e){
        this.setState({
            onSearch: e.target.value
        })
    }

    clearSearch(){
        this.setState({
            onSearch: "",
            needClearSearch: false
        })
    }

    closeSearchMessage() {
        this.setState({
            searchResultMessage: ""
        })
    }

    render(){
        return(
            <div className="container">
                {
                    this.state.message==='' ? 
                ''
                :
                <div className="message-box">
                    <p>{this.state.message}      <i className="far fa-times-circle" onClick={this.messageSwitch.bind(this)}/></p>
                </div>

                }
                {
                    this.state.isLoading ?
                    <p>Loading...</p>
                    :
                    <div>
                        <div>
                            <form  onSubmit={this.handleOnSearch.bind(this)}>
                                <label>Search Course : </label>
                                <input value={this.state.onSearch} placeholder="Course Name/Id" onChange={this.searchChanged.bind(this)} />
                                {
                                    this.state.needClearSearch ?
                                    <div className="clear-search" onClick={this.clearSearch.bind(this)}>Clear Search</div>
                                    :
                                    <button className="more-button ml-2" type="submit" >Search</button>
                                }
                            </form>
                            {
                                this.state.searchResultMessage === "" ?
                                ''
                                :
                                <p className="message-box">{this.state.searchResultMessage}<i className="far fa-times-circle" onClick={this.closeSearchMessage.bind(this)}/></p>
                            }
                        </div>
                        <div className="row">
                            <div className="col-sm adjust-container">
                                <div className="highlight-header">Assigned Courses :</div>
                                <DisplayCourses courses={this.state.enrolledCourses} loadDetail={this.loadDetail.bind(this)}/>
                                <div className="highlight-header">Courses List :</div>
                                <DisplayCourses courses={this.state.allCourses} loadDetail={this.loadDetail.bind(this)}/>
                            </div>
                            <div className="col-sm">
                            { 
                                this.state.onClickCourse==="" ?
                                <div className="text-center text-primary adjust-container">
                                    <div className="see-detail">Click More Button To See Detail</div>
                                    <img className="course-image" src="https://us.123rf.com/450wm/microone/microone1702/microone170200165/71549666-teacher-professor-standing-in-front-of-blank-school-blackboard-vector-illustration-school-teacher-in.jpg?ver=6" alt="course" />
                                </div>
                                :
                                <CourseDetail courseDetail={this.state.courseDetail}
                                            displayStudentsList={this.state.displayStudentsList}
                                            onRefresh={this.loadDetail.bind(this)} 
                                            onClickCourse={this.state.onClickCourse}
                                            onEditing={this.onEditing.bind(this)}/>
                            }
                            </div>
                        </div>
                    </div>
                }
            </div>
        );  
    }
}


function DisplayCourses(props){
    let rows = [];
    rows = props.courses.map(course=>{
        return <Course courseName={course.CourseName} loadDetail={props.loadDetail} id={course.Id} key={course.Id} />
    })
    return <div><ul>{rows}</ul></div>
}


function Course(props){
    return(
        <li className="space-between-container detail-item">
            <span><b>Course Name:</b> {props.courseName}</span>
            <span><b>Course Id:</b> {props.id}</span> 
            <button className="more-button" onClick={()=>props.loadDetail(props.id)}>More</button>
        </li>
    )
}












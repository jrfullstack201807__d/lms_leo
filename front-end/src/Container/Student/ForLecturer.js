import React,{Component} from 'react';
import CourseDetail from './CourseDetail';
import student from '../../image/marsdenstudent.jpg'
import {getCoursesForLecturer} from '../../API/LMS'


export default class ForLecturer extends Component{
   constructor(props){
       super(props);
       this.state={
            enrolledCourses: [],
            searchResultMessage: "",
            onSearch: "",
            needClearSearch: false
       }
   }

   lecturerId = this.props.userInfo.EnrolledId;
   token = sessionStorage.getItem('key');

   componentDidMount(){
    this.getCourses();
    }

    getCourses(){
        getCoursesForLecturer(this.token,this.lecturerId).then(data=>this.handleFetchData(data)).catch(function (error) {
            console.log(error);
        })
    }
    
    handleFetchData(data){
        //console.log(data)
        this.setState({
            enrolledCourses: data
        })
    }

    handleOnSearch(e){
        e.preventDefault();
        const selectedCourse = this.state.enrolledCourses.filter(course=>{
            return course.Course.CourseName.toLowerCase() === this.state.onSearch.toLowerCase() || course.Course.Id === parseInt(this.state.onSearch)
        });
        if(selectedCourse.length>0){
            this.setState({
                enrolledCourses: selectedCourse,
                needClearSearch: true,
                searchResultMessage: ""
            })
        }else{
            this.setState({
                onSearch: "",
                searchResultMessage: "Course Name or Course Id does not exist"
            })
        }
    }

    searchChanged(e){
        this.setState({
            onSearch: e.target.value
        })
    }

    clearSearch(){
        this.setState({
            onSearch: "",
            needClearSearch: false
        })
        this.getCourses();
    }

    closeSearchMessage() {
        this.setState({
            searchResultMessage: ""
        })
    }

    render(){
        return(
            <div>
                <div><img className="marsden-student" src={student} alt="student" /></div>
                <div>
                    <form  onSubmit={this.handleOnSearch.bind(this)}>
                        <label>Search Course : </label>
                        <input value={this.state.onSearch} placeholder="Course Name/Id" onChange={this.searchChanged.bind(this)} />
                        {
                            this.state.needClearSearch ?
                            <div className="clear-search" onClick={this.clearSearch.bind(this)}>Clear Search</div>
                            :
                            <button className="more-button ml-2" type="submit" >Search</button>
                        }
                    </form>
                    {
                        this.state.searchResultMessage === "" ?
                        ''
                        :
                        <p className="message-box">{this.state.searchResultMessage}<i className="far fa-times-circle" onClick={this.closeSearchMessage.bind(this)}/></p>
                    }
                </div>
                <div className="container">
                    <CoursesList enrolledCourses={this.state.enrolledCourses}/>
                </div>
            </div>
        );  
    }
}

function CoursesList(props){
    let rows = [];
    rows = props.enrolledCourses.map(course=>{
        return <CourseDetail courseName={course.Course.CourseName} courseId={course.CourseId} key={course.CourseId}/>
    })
    return <div className="row">{rows}</div>
}

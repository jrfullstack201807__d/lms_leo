import React,{Component} from 'react';
import {withRouter} from 'react-router-dom'
import {getAllCourse,studentEnrollCourse,studentDropCourse} from '../../API/LMS'

class ForStudent extends Component{
    constructor(props){
        super(props);
        this.state={
            isLoading: true,
            enrolledCourses: [],
            availableCourses: []
        }
    }

    studentId = this.props.userInfo.EnrolledId;
    token = sessionStorage.getItem('key');

    componentDidMount(){
        this.getCourses();
    }

    getCourses(){
        getAllCourse(this.token).then(data=>this.arrayFilter(data)).catch(function (error) {
            console.log(error);
          });
    }
    

    arrayFilter(data){
        let enrolledArray= [];
        let availableArray = [];
        //console.log(data);

        enrolledArray = data.filter(course=>{
            return  this.isEnrolled(course.StudentsCourses)})

            availableArray = data.filter(course=>{
            return   !this.isEnrolled(course.StudentsCourses)})

        //console.log(enrolledArray);
        //console.log(availableArray);
        this.setState({
            isLoading: false,
            enrolledCourses: enrolledArray,
            availableCourses: availableArray
        })
    }

    isEnrolled(courses){
        return   courses.some( item => item.StudentId === this.studentId ) 
    }

    switchEnrollStatus(courseId,isEnrolled){
        if(isEnrolled){
            studentDropCourse(this.token,this.studentId,courseId).then(data=>this.arrayFilter(data)).catch(function (error) {
                console.log(error);
              });
        }else{
            studentEnrollCourse(this.token,this.studentId,courseId).then(data=>this.arrayFilter(data)).catch(function (error) {
                
                try {
                    if(error.response.status===404){
                    alert("Error, You Don't Have Enough Credit Left")
                    }
                }catch(e) {
                }finally {
                    console.log(error);
                }
              });
        }
        
    }
   

    render(){
        return(
            <div className="detail-box container">
                {
                    this.state.isLoading ?
                    <p>Loading...</p>
                    :
                    <div className="row">
                        <div className="col rounded shadow detail-item-container ml-3 mt-5">
                            <div className="highlight-header"><h4>Enrolled Courses :</h4></div>
                            <Rows courses={this.state.enrolledCourses} isEnrolled={true} switchEnrollStatus={this.switchEnrollStatus.bind(this)}/>
                        </div>
                        <div className="col rounded shadow detail-item-container ml-3 mt-5">
                            <div className="highlight-header"><h4>Available Courses :</h4></div>
                            <Rows courses={this.state.availableCourses} isEnrolled={false} switchEnrollStatus={this.switchEnrollStatus.bind(this)}/>
                        </div>
                    </div>
                }
            </div>
        );  
    }
}

function Rows(props){
    let rows = [];
    rows = props.courses.map(course=>{
        return <Display courseName={course.CourseName} id={course.Id} credit={course.Credit} key={course.Id} isEnrolled={props.isEnrolled} switchEnrollStatus={(courseId)=>props.switchEnrollStatus(courseId,props.isEnrolled)}/>
    })
    
    return <div>{props.courses.length>0 ?<div>{rows}</div>:<div className="text-center"><p>It's Empty!</p><img className="empty-image" src="https://cdn.dribbble.com/users/634336/screenshots/2246883/_____.png" alt="empty"/></div>}</div>
}


function Display(props){
    return(
        <div className="detail-item">
            <div className="space-between-container">
                <span><b>Course Name:</b> {props.courseName}</span>
                <span><b>Course Id:</b> {props.id}</span>
                <span><b>Course Credit:</b> {props.credit}</span>
            </div>
            <div>
            {
                props.isEnrolled ? 
                <button className="delete-button" onClick={()=>props.switchEnrollStatus(props.id)}>Drop Course</button> 
                : 
                <button className="add-button" onClick={()=>props.switchEnrollStatus(props.id)}>Enroll Course</button>
            }
            </div>
        </div>
    )
}

export default withRouter(ForStudent)

import React,{Component} from 'react';
import book from '../../image/book.png'
import {getStudentsForCourse} from '../../API/LMS';


export default class CourseName extends Component{
    constructor(props){
        super(props);
        this.state={
            displayList: false,
            studentsList:[]
        }
    }

    token = sessionStorage.getItem('key');

    componentDidMount(){
        this.getStudents();
    }

    getStudents(){
        getStudentsForCourse(this.token,this.props.courseId).then(data=>this.handleFetchData(data)).catch(function (error) {
            console.log(error);
        })
    }
    
    handleFetchData(data){
        // console.log(data)
        this.setState({
            studentsList: data
        })
    }

    switchDisplayList(){
        this.setState({
            displayList: !this.state.displayList
        })
    }

    

    render(){
    return <div className="detail-box col col-sm-4 text-center">
                <img onClick={this.switchDisplayList.bind(this)} className="personal-icon" src={book} alt="book"/>
                <div onClick={this.switchDisplayList.bind(this)}><h4 className="text-primary"> {this.props.courseName}</h4><p>  Click To See Students List</p></div>
            {
                this.state.displayList ?
                <DisplayList studentsList={this.state.studentsList}/>
                :
                ''
            }
            </div>

    }
}

function DisplayList(props){
    let rows = [];
    rows = props.studentsList.map(student=>{
        return <DisplayStudent fullName={student.Student.FullName} iconURL={student.Student.IconURL} key={student.StudentId}/>
    })
    return <div>
        {
        props.studentsList.length === 0 ?
        <div><b>Student List Is Empty</b><img className="empty-image" src="https://cdn.dribbble.com/users/634336/screenshots/2246883/_____.png" alt="empty"/></div>
        :
        <ul className="p-0">{rows}</ul>
        }
        </div>
}

function DisplayStudent(props){
    return <li><img className="personal-icon" src={props.iconURL} alt="student-icon" /><p>{props.fullName}</p></li>
}
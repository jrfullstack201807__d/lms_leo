import React from 'react';
import ForStudent from './ForStudent';
import ForLecturer from './ForLecturer';
import ForAdmin from './ForAdmin';

export default function StudentContainer (props){
    return(
        <div className="non-img-container">
            <h1>- Student -</h1>
            {props.userInfo.UserType==="Student" ? <ForStudent userInfo={props.userInfo}/> : ''}
            {props.userInfo.UserType==="Lecturer" ? <ForLecturer userInfo={props.userInfo}/> : ''}
            {props.userInfo.UserType==="Admin" ? <ForAdmin userInfo={props.userInfo}/> : ''}
        </div>
    );  
}
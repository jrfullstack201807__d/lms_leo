import React,{Component} from 'react';

export default class StudentDetail extends Component{
    constructor(props){
        super(props);
        this.state={
            displayDetail: false,
            onRemoveOrDelete: false
        }
    }

    detail=this.props.studentDetail.UserDetail;
    studentId=this.detail.EnrolledId
    userId=this.detail.Id

    onSwitchDisplay(){
        this.setState({
            displayDetail: !this.state.displayDetail
        })
    }

    onRemoveOrDelete(){
        this.setState({
            onRemoveOrDelete: !this.state.onRemoveOrDelete
        })
    }

    onDeleteUser(){
        this.props.onDeleteUser(this.userId);
    }
    onRemoveStudent(){
        this.props.onRemoveStudent(this.studentId);
    }

    render(){
        return (
            <div className="text-center col col-sm-6">
            <img  className="personal-icon" src={this.props.iconURL} alt="student-icon" onClick={this.onSwitchDisplay.bind(this)}/>
            {
                this.state.displayDetail ? 
                <div className="rounded shadow bg-img-container">
                    {
                        this.state.onRemoveOrDelete ?
                        <div>
                            <button className="delete-secondary-button" onClick={this.onRemoveStudent.bind(this)}>Remove Student</button>
                            <p className="message-box">This will remove user 's information about Enrollment </p>
                            <button className="delete-button" onClick={this.onDeleteUser.bind(this)}>Delete User</button>
                            <p className="message-box">User will be DELETE, all information no longer exists</p>
                            <button className="add-button" onClick={this.onRemoveOrDelete.bind(this)}>Cancel</button>
                        </div>
                        :
                        <div>
                            <div className="message-box text-center text-primary" onClick={this.onSwitchDisplay.bind(this)}><b>{this.props.studentDetail.FullName}</b></div>
                            <p><b>Student Id:</b> {this.detail.EnrolledId}</p>
                            <p><b>User Id:</b> {this.detail.Id}</p>
                            <p><b>Email:</b> {this.detail.Email_Activated ? this.detail.Email : "Unactivated"}</p>
                            <p><b>Phone:</b> {this.detail.Phone_Activated ? this.detail.Phone : "Unactivated"}</p>
                            <button className="delete-button" onClick={this.onRemoveOrDelete.bind(this)}>Remove / Delete</button>
                        </div>
                    }
                </div>
                :
                <h4 className="text-primary" onClick={this.onSwitchDisplay.bind(this)}>{this.props.studentDetail.FullName}</h4>
            }
            
            </div>
        )
    }
}

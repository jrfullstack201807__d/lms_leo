import React,{Component} from 'react';
import StudentDetail from './StudentDetail';
import {getAllStudents,deleteStudent,deleteStudentUser} from '../../API/LMS';

export default class ForAdmin extends Component{

    constructor(props){
        super(props);
        this.state={
            isLoading: true,
            allStudents: [],
            searchResultMessage: "",
            onSearch: "",
            needClearSearch: false
        }
    }

    token = sessionStorage.getItem('key');

    componentDidMount(){
        this.getStudents();
    }

    getStudents(){
        getAllStudents(this.token).then(data=>this.handleFetchData(data)).catch(function (error) {
            console.log(error);
          });
    }


    handleFetchData(data){
        //console.log(data)
        this.setState({
            isLoading: false,
            allStudents: data
        })
    }

    onRemoveStudent(studentId){
        deleteStudent(this.token,studentId).then(data=>this.handleFetchData(data)).catch(function (error) {
            console.log(error);
          });
    }

    onDeleteUser(userId){
        deleteStudentUser(this.token,userId).then(data=>this.handleFetchData(data)).catch(function (error) {
            console.log(error);
          });
    }

    handleOnSearch(e){
        e.preventDefault();
        const selectedstudent = this.state.allStudents.filter(student=>{
            return student.FullName.toLowerCase() === this.state.onSearch.toLowerCase() || student.Id === parseInt(this.state.onSearch)
        });
        if(selectedstudent.length>0){
            this.setState({
                allStudents: selectedstudent,
                needClearSearch: true,
                searchResultMessage: ""
            })
        }else{
            this.setState({
                onSearch: "",
                searchResultMessage: "Student Name or Student Id does not exist"
            })
        }
    }

    searchChanged(e){
        this.setState({
            onSearch: e.target.value
        })
    }

    clearSearch(){
        this.setState({
            onSearch: "",
            needClearSearch: false
        })
        this.getStudents();
    }

    closeSearchMessage() {
        this.setState({
            searchResultMessage: ""
        })
    }

    render(){
        return(
            <div>
                {
                    this.state.isLoading ?
                    <p>Loading...</p>
                    :
                    <div>
                        <div>
                            <form  onSubmit={this.handleOnSearch.bind(this)}>
                                <label>Search Student : </label>
                                <input value={this.state.onSearch} placeholder="Student Name/Id" onChange={this.searchChanged.bind(this)} />
                                {
                                    this.state.needClearSearch ?
                                    <div className="clear-search" onClick={this.clearSearch.bind(this)}>Clear Search</div>
                                    :
                                    <button className="more-button ml-2" type="submit" >Search</button>
                                }
                            </form>
                            {
                                this.state.searchResultMessage === "" ?
                                ''
                                :
                                <p className="message-box">{this.state.searchResultMessage}<i className="far fa-times-circle" onClick={this.closeSearchMessage.bind(this)}/></p>
                            }
                        </div>
                        <div className="container"><StudentsList students={this.state.allStudents} onDeleteUser={this.onDeleteUser.bind(this)} onRemoveStudent={this.onRemoveStudent.bind(this)}/></div>
                    </div>
                }
            </div>
        ); 
    }
}

function StudentsList(props){
    let rows = []
    rows = props.students.map(student=>{
        return <StudentDetail studentDetail={student} key={student.UserId} onDeleteUser={props.onDeleteUser} iconURL={student.IconURL} onRemoveStudent={props.onRemoveStudent}/>
    })
    return <div className="detail-box row">
                {rows}
            </div>
}

import React,{Component} from 'react';
import {getAllLecturer,getCoursesForLecturer} from '../../API/LMS'
export default class ForStudent extends Component{

    constructor(props){
        super(props);
        this.state={
            isLoading: true,
            onClickLecturer: "",
            allLecturer: [],
            lecturerDetail: [],
            onClickLecturerIcon: "",
            searchResultMessage: "",
            onSearch: "",
            needClearSearch: false
        }
    }

    token = sessionStorage.getItem('key');

    componentDidMount(){
        this.getLecturers();
    }

    getLecturers(){
        getAllLecturer(this.token).then(data=>this.handleFetchData(data)).catch(function (error) {
            console.log(error);
          });
    }

    handleFetchData(data){
        //console.log(data)
        this.setState({
            isLoading: false,
            allLecturer: data
        })
    }

    loadDetail(lecturerId){
        getCoursesForLecturer(this.token,lecturerId).then(data=>this.handleLecturerDetail(data,lecturerId)).catch(function (error) {
            console.log(error);
        })
    }

    handleLecturerDetail(data,lecturerId){
        let lecturer = this.state.allLecturer.find(function(lecturer) {
            return lecturer.Id === lecturerId
        })
        this.setState({
            onClickLecturer: lecturer.FullName,
            lecturerDetail: data,
            onClickLecturerIcon: lecturer.IconURL
        })
    }

    handleOnSearch(e){
        e.preventDefault();
        const selectedLecturer = this.state.allLecturer.find(lecturer=>{
            return lecturer.FullName.toLowerCase() === this.state.onSearch.toLowerCase() || lecturer.Id === parseInt(this.state.onSearch)
        });
        if(selectedLecturer !== undefined){
            console.log(selectedLecturer)
            this.loadDetail(selectedLecturer.Id);
            this.setState({
                needClearSearch: true,
                searchResultMessage: ""
            })
        }else{
            this.setState({
                onSearch: "",
                searchResultMessage: "Lecturer Name or Lecturer Id does not exist"
            })
        }
    }

    searchChanged(e){
        this.setState({
            onSearch: e.target.value
        })
    }

    clearSearch(){
        this.setState({
            onClickLecturer: "",
            onSearch: "",
            needClearSearch: false
        })
    }

    closeSearchMessage() {
        this.setState({
            searchResultMessage: ""
        })
    }


    render(){
        return(
            <div className="container">
                {
                    this.state.isLoading ?
                    <p>Loading...</p>
                    :
                    <div>
                        <div>
                            <form  onSubmit={this.handleOnSearch.bind(this)}>
                                <label>Search Lecturer : </label>
                                <input value={this.state.onSearch} placeholder="Lecturer Name/Id" onChange={this.searchChanged.bind(this)} />
                                {
                                    this.state.needClearSearch ?
                                    <div className="clear-search" onClick={this.clearSearch.bind(this)}>Clear Search</div>
                                    :
                                    <button className="more-button ml-2" type="submit" >Search</button>
                                }
                            </form>
                            {
                                this.state.searchResultMessage === "" ?
                                ''
                                :
                                <p className="message-box">{this.state.searchResultMessage}<i className="far fa-times-circle" onClick={this.closeSearchMessage.bind(this)}/></p>
                            }
                        </div>
                        <div className="row">
                            <div className="col-lg rounded shadow bg-img-container ml-3 mt-5">
                                <div className="highlight-header">Lecturers List :</div>
                                <LecturersList lecturers={this.state.allLecturer} loadDetail={this.loadDetail.bind(this)}/>
                            </div>
                            <div className="col-lg">
                            {
                                this.state.onClickLecturer==="" ?
                                <div className="text-center text-primary adjust-container rounded shadow ml-3 mt-5">
                                    <div className="see-detail">Click More Button To See Detail</div>
                                    <img className="course-image" src="https://image.freepik.com/free-vector/teacher-with-a-blackboard-design_1214-222.jpg" alt="course" />
                                </div>
                                :
                                <div className="bg-img-container  rounded shadow ml-3 mt-5">
                                    <DisplayDetail lecturerDetail={this.state.lecturerDetail} onClickLecturer={this.state.onClickLecturer} onClickLecturerIcon={this.state.onClickLecturerIcon}/>
                                </div>
                            }
                            </div>
                        </div>
                    </div>
                }
            </div>
        ); 
    }
}

function DisplayDetail(props){
    let rows = []
    rows = props.lecturerDetail.map(lecturer=>{
        return <CourseName  course={lecturer.Course} key={lecturer.CourseId}/>
    })
    return <div>
                {
                    props.lecturerDetail.length > 0 ?
                    
                    <div>
                    <h3 className="highlight-header">Lecturer - {props.onClickLecturer} Assigned {props.lecturerDetail.length} Courses</h3>
                    <div className=" text-center"><img className="personal-icon" src={props.onClickLecturerIcon} alt="lecturer-icon"/></div>
                    <ul>{rows}</ul>
                    </div>
                    :
                    <div>
                    <h2 className="highligh-header">Courses Unassigned</h2>
                    <p>{props.onClickLecturer} currently has 0 course</p>
                    </div>
                }
            </div>
}

function CourseName(props){
    let courseName = props.course.CourseName;
    return <li><h4>{courseName}</h4></li>
}

function LecturersList(props){
    let rows = [];
    rows = props.lecturers.map(lecturer=>{
        return <Lecturer fullName={lecturer.FullName} id={lecturer.Id} loadDetail={props.loadDetail} key={lecturer.Id}/>
    })
    return <div><ul>{rows}</ul></div>
}


function Lecturer(props){
    return(
        <li className="space-between-container detail-item">
            <span><b>Lecturer Name:</b>  {props.fullName} </span>
            <span><b>Lecturer Id:</b>  {props.id} </span>
            <button className="more-button" onClick={()=>props.loadDetail(props.id)}>More</button>
        </li>
    )
}
    

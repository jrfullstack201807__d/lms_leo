import React,{Component} from 'react';
import LecturerDetail from './LecturerDetail';
import {getAllLecturer,deleteLecturer,deleteLecturerUser} from '../../API/LMS';


export default class ForAdmin extends Component{

    constructor(props){
        super(props);
        this.state={
            isLoading: true,
            allLecturers: [],
            searchResultMessage: "",
            onSearch: "",
            needClearSearch: false
        }
    }

    token = sessionStorage.getItem('key');

    componentDidMount(){
        this.getLecturers();
    }

    getLecturers(){
        getAllLecturer(this.token).then(data=>this.handleFetchData(data)).catch(function (error) {
            console.log(error);
          });
    }

    handleFetchData(data){
        console.log(data)
        this.setState({
            isLoading: false,
            allLecturers: data
        })
    }

    onRemoveLecturer(lecturerId){
        deleteLecturer(this.token,lecturerId).then(data=>this.handleFetchData(data)).catch(function (error) {
            console.log(error);
          });
    }


    onDeleteUser(userId){
        deleteLecturerUser(this.token,userId).then(data=>this.handleFetchData(data)).catch(function (error) {
            console.log(error);
          });
    }

    handleOnSearch(e){
        e.preventDefault();
        console.log(this.state.allLecturers)
        const selectedLecturer = this.state.allLecturers.filter(lecturer=>{
            return lecturer.FullName.toLowerCase() === this.state.onSearch.toLowerCase() || lecturer.Id === parseInt(this.state.onSearch)
        });
        if(selectedLecturer.length>0){
            this.setState({
                allLecturers: selectedLecturer,
                needClearSearch: true,
                searchResultMessage: ""
            })
        }else{
            this.setState({
                onSearch: "",
                searchResultMessage: "Lecturer Name or Lecturer Id does not exist"
            })
        }
    }

    searchChanged(e){
        this.setState({
            onSearch: e.target.value
        })
    }

    clearSearch(){
        this.setState({
            onSearch: "",
            needClearSearch: false
        })
        this.getLecturers();
    }

    closeSearchMessage() {
        this.setState({
            searchResultMessage: ""
        })
    }

    render(){
        return(
            <div>
                {
                    this.state.isLoading ?
                    <p>Loading...</p>
                    :
                    <div>
                        <div>
                            <form  onSubmit={this.handleOnSearch.bind(this)}>
                                <label>Search Lecturer : </label>
                                <input value={this.state.onSearch} placeholder="Lecturer Name/Id" onChange={this.searchChanged.bind(this)} />
                                {
                                    this.state.needClearSearch ?
                                    <div className="clear-search" onClick={this.clearSearch.bind(this)}>Clear Search</div>
                                    :
                                    <button className="more-button ml-2" type="submit" >Search</button>
                                }
                            </form>
                            {
                                this.state.searchResultMessage === "" ?
                                ''
                                :
                                <p className="message-box">{this.state.searchResultMessage}<i className="far fa-times-circle" onClick={this.closeSearchMessage.bind(this)}/></p>
                            }
                        </div>
                        <div className="container"><LecturersList lecturers={this.state.allLecturers} onDeleteUser={this.onDeleteUser.bind(this)} onRemoveLecturer={this.onRemoveLecturer.bind(this)}/></div>
                    </div>
                }
            </div>
        ); 
    }
}

function LecturersList(props){
    let rows = []
    rows = props.lecturers.map(lecturer=>{
        return <LecturerDetail lecturerDetail={lecturer} key={lecturer.UserId} onDeleteUser={props.onDeleteUser} iconURL={lecturer.IconURL} onRemoveLecturer={props.onRemoveLecturer}/>
    })
    return <div className="detail-box row">
                {rows}
            </div>
}
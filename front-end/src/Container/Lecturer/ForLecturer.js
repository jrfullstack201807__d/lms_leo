import React,{Component} from 'react';
import {getAllCourse,lecturerEnrollCourse,lecturerDropCourse,addCourse} from '../../API/LMS'
import '../../Home/UnauthHome/Login/Login.css'

export default class ForLecturer extends Component{
    constructor(props){
        super(props);
        this.state={
            isLoading: true,
            displayMessage: false,
            message: "",
            submit: "submit",
            courseCredit: 0,
            courseDescription: "",
            newCourse: "",
            enrolledCourses: [],
            availableCourses: []
        }
    }

    lecturerId = this.props.userInfo.EnrolledId;
    token = sessionStorage.getItem('key');

    componentDidMount(){
        this.getCourses();
    }

    getCourses(){
        getAllCourse(this.token).then(data=>this.arrayFilter(data)).catch(function (error) {
            console.log(error);
          });
    }
    
    arrayFilter(data){
        let enrolledArray= [];
        let availableArray = [];

        enrolledArray = data.filter(course=>{
            return  this.isEnrolled(course.LecturersCourses)})

        availableArray = data.filter(course=>{
            return   !this.isEnrolled(course.LecturersCourses)})

        this.setState({
            isLoading: false,
            enrolledCourses: enrolledArray,
            availableCourses: availableArray
        })
    }


    isEnrolled(lecturersCourses){
        return   lecturersCourses.some( item => item.LecturerId === this.lecturerId ) 
    }


    switchEnrollStatus(courseId,isEnrolled){
        if(isEnrolled){
            lecturerDropCourse(this.token,this.lecturerId,courseId).then(data=>this.arrayFilter(data)).catch(function (error) {
                console.log(error);
              });
        }else{
            lecturerEnrollCourse(this.token,this.lecturerId,courseId).then(data=>this.arrayFilter(data)).catch(function (error) {
                try {
                    if(error.response.status===404){
                    alert('Error, Please Fill Out Personal Detail First')
                    }
                }catch(e) {
                }finally {
                    console.log(error);
                }
              });
        }
        
    }

    onAddNewCourse(e){
        this.setState({
            newCourse: e.target.value
        })
    }

    onDescrptionChange(e){
        this.setState({
            courseDescription: e.target.value
        })
    }

    handleSubmit(e){
        e.preventDefault();
        this.setState({submit:"button"})
        let course = {
            Id: 0,
            Credit: this.state.courseCredit,
            CourseName: this.state.newCourse,
            Description: this.state.courseDescription
        }
        addCourse(this.token,course).then(data=>this.handleNewCourse(data)).catch(function (error) {
            console.log(error);
        });
        this.setState({
            submit:"submit",
            newCourse: "",
            courseDescription: ""
        })
    }

    handleNewCourse(data){
        if(data.Status===1){
            this.getCourses();
        }
        this.setState({
            message: data.Message,
            displayMessage: true,
        })
    }

    messageSwitch(){
        this.setState({
            displayMessage: false,
        })
    }

    onSelectChange(e){
        this.setState({courseCredit:e.target.value})
    }

    render(){
        return(
            <div>
                {
                    this.state.isLoading ?
                    <p>Loading...</p>
                    :
                    <div className="container">
                        {
                            this.state.displayMessage ?
                            <div className="message-box">
                                <p>{this.state.message}      <i className="far fa-times-circle" onClick={this.messageSwitch.bind(this)}/></p>
                                
                            </div>
                            :
                            ''
                        }
                        <form className="bg-img-container rounded shadow" onSubmit={this.handleSubmit.bind(this)}>
                            <h4 className="text-center text-primary">Add New Course</h4>
                            <div className="space-between-container">
                            <label>Enter New Course : </label>
                            <input value={this.state.newCourse} placeholder="Course Name" onChange={this.onAddNewCourse.bind(this)}/>
                            <label>Course Credit: </label>
                            <select onChange={this.onSelectChange.bind(this)}>
                                <option value="0">0</option>
                                <option value="1">1</option>
                                <option value="2">2</option>
                                <option value="3">3</option>
                            </select>
                            <label>Description : </label>
                            <textarea value={this.state.courseDescription} placeholder="Enter Course Description" onChange={this.onDescrptionChange.bind(this)} />
                            <button type={this.state.submit}>Submit</button>
                            </div>
                        </form>
                        <div className="row">
                            <div className="col rounded shadow detail-item-container ml-3 mt-5">
                                <div className="highlight-header">Assigned Courses :</div>
                                <Rows courses={this.state.enrolledCourses} isEnrolled={true} switchEnrollStatus={this.switchEnrollStatus.bind(this)}/>
                            </div>
                            <div className="col rounded shadow detail-item-container ml-3 mt-5">
                                <div className="highlight-header">Available Courses :</div>
                                <Rows courses={this.state.availableCourses} isEnrolled={false} switchEnrollStatus={this.switchEnrollStatus.bind(this)}/>
                            </div>
                        </div>
                    </div>
                }
            </div>
        );  
    }
}
function Rows(props){
    let rows = [];
    rows = props.courses.map(course=>{
        return <Display courseName={course.CourseName} id={course.Id} key={course.Id} isEnrolled={props.isEnrolled} switchEnrollStatus={(courseId)=>props.switchEnrollStatus(courseId,props.isEnrolled)}/>
    })
    return<div>{props.courses.length>0 ?<div>{rows}</div>:<div className="text-center"><p>It's Empty!</p><img className="empty-image" src="https://cdn.dribbble.com/users/634336/screenshots/2246883/_____.png" alt="empty"/></div>}</div>
}


function Display(props){
    return(
        <div className="detail-item">
            <div className="space-between-container">
                <span><b>Course Name:</b> {props.courseName} </span>
                <span><b>Course Id:</b> {props.id} </span>
            </div>
            {props.isEnrolled ? 
            <button className="delete-button" onClick={()=>props.switchEnrollStatus(props.id)}>Drop Course</button> 
            : 
            <button className="add-button" onClick={()=>props.switchEnrollStatus(props.id)}>Assign Course</button>
            }
        </div>
    )
}
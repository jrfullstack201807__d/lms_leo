import React,{Component} from 'react';
import {NavLink,withRouter} from 'react-router-dom';
import './Welcome.css';

import Budget from '../image/RAM_Web_Banner.jpg';
import Scholarships from '../image/scholarshipsimage.png';
import Uniform from '../image/UNIFORM_SHOP_SIGN.jpg';
import Course from '../image/course.jpg';
import Student from '../image/student.jpg';
import Lecturer from '../image/lecturer.jpg';

class Welcome extends Component{
    userName = this.props.userInfo.Name
    courseUrl = '/' + this.userName + '/course';
    studentUrl = '/' + this.userName + '/student';
    lecturerUrl = '/' + this.userName + '/lecturer';
    detailUrl = '/' + this.userName + '/detail'

    isUserEnrolled = this.props.userInfo.IsEnrolled;
    isEmailConfirmed = this.props.userInfo.EmailConfirmed

    onRedirect(){
        this.props.history.push(this.detailUrl);
    }

    render(){
        return(
            <div>
                {
                    this.isUserEnrolled && this.isEmailConfirmed ?
                    <h1 className="alert alert-warning">Hello {this.userName}, Welcome to LMS-{this.props.userInfo.UserType}</h1>
                    :
                    <div className="rounded shadow bg-img-container">
                        <p>Hi {this.userName}, LMS notices that you haven't filled out the following detail： <br/>{this.isUserEnrolled || this.props.userInfo.UserType === "Admin" ? '':<b>Enrollment</b>} <br/>{this.isEmailConfirmed ? '':<b>Email Validation</b>}</p>
                        <button className="btn btn-success" onClick={this.onRedirect.bind(this)}> Fill Out Detail </button>
                    </div>  
                }
                <div className="container">
                    <div className="row justify-content-around">
                        <div className="img-nav-wrapper col-lg-4">
                        <h2>Course</h2>
                            <NavLink to={this.courseUrl}><img  className="welcome-image" src={Course} alt="course"/></NavLink>
                        </div>
                        <div className="img-nav-wrapper col-lg-4">
                        <h2>Student</h2>
                            <NavLink to={this.studentUrl}><img  className="welcome-image" src={Student} alt="student"/></NavLink>
                        </div>
                        <div className="img-nav-wrapper col-lg-4">
                        <h2>Lecturer</h2>
                            <NavLink to={this.lecturerUrl}><img  className="welcome-image" src={Lecturer} alt="lecturer"/></NavLink>
                        </div>
                    </div>
                    
                    <div className="text-center slogan">
                        <h2 className="text-secondary"><b>Welcome To Marsden LMS</b></h2>
                        <p>Learning Today, Shaping Tomorrow</p>
                        <p><b>T</b>: 02 9874 6544</p><p><b>E</b>: Emailmarsden-h.school@det.nsw.edu.au</p>
                    </div>

                    <div className="row">
                        <img className="col-sm-6 home-image" src={Budget} alt="budget"/>
                        <div className="col-sm-6 image-intro">
                        <div>
                            <h4><b>2019 school budget allocations</b></h4>
                            <p>12 Nov 2018</p>
                            <p>This week we will receive a School Budget Allocation Report with our funding for 2019</p>
                        </div>
                        </div>
                    </div>
                    <div className="row">
                        <div className="col-sm-6 image-intro order-last order-sm-first">
                        <div>
                            <h4><b>Uniform Shop OPEN this SATURDAY</b></h4>
                            <p>14 Nov 2018</p>
                            <p>P&C Uniform Shop</p>
                        </div>
                        </div>
                        <img className="col-sm-6 home-image order-first order-sm-last" src={Uniform} alt="Uniform"/>
                    </div>
                    <div className="row">
                        <img className="col-sm-6 home-image" src={Scholarships} alt="Scholarships"/>
                        <div className="col-sm-6 image-intro">
                        <div>
                            <h4><b>2019 Scholarship OPEN</b></h4>
                            <p>25 Oct 2018</p>
                            <p>2019 Scholarship applications are now OPEN</p>
                        </div>
                        </div>
                    </div>
                </div>
            </div>
        );  
    }
}

export default withRouter(Welcome);
import React,{Component} from 'react';
import {postIcon} from '../API/LMS';

export default class UploadIcon extends Component{
    constructor(props){
        super(props);
        this.state={
            preview: null,
            selectedImageFile: null
        }
    }

    // token = "Bearer " + sessionStorage.getItem('key');
    token = sessionStorage.getItem('key');

    handleFileChange(e) {
        const file = e.target.files[0]
        if (file) {
          if (file.size <= 2048000) {
            var src = window.URL.createObjectURL(file)
            var preview = <img src={src} alt='personal-icon' className="personal-icon" />
            this.setState({
              selectedImageFile: file,
              preview: preview
            })
          } else {
            let data={
                Status: 0,
                Message: "Error, image over size. Make sure the file under 2Mb"
            }
            this.handleResult(data);
            this.cancel();
          }
        }
    }

    handleResult(res) {
        this.props.handleUploadResult(res);
    }

    upload() {
        const selectedImageFile = this.state.selectedImageFile;
        if (!selectedImageFile) {
            let data={
                Status: 0,
                Message: "Error, Please choose a photo first!"
            }
            this.handleResult(data);
            return;
        }
        postIcon(this.token, selectedImageFile).then(data=>this.handleResult(data)).catch(function (error) {
            console.log(error);
        });
    }

    cancel() {
        this.setState({
            preview: null,
            selectedImageFile: null
        })
    }
    
    render(){
        const { preview } = this.state;
        return(
            <div className="rounded shadow add-icon-wrapper ">
                <h4 className="text-center text-primary">Upload Icon</h4>
                <div className="text-center">
                    {preview}
                </div>
                <div className="text-center">
                    <input type='file' accept="image/jpeg,image/jpg,image/png" onChange={this.handleFileChange.bind(this)} />
                    <button className="btn btn-primary" onClick={this.upload.bind(this)}>upload</button>
                    <button  className="btn btn-secondary ml-1" onClick={this.cancel.bind(this)}>cancel</button>
                </div>
            </div>
        )
    }
}
